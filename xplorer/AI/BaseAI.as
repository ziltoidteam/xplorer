﻿package xplorer.AI {
    import xplorer.system.Vector2d;
    import xplorer.game.objects.SpaceObject;
    import xplorer.game.objects.Ship;

	/**
	 *  This class is base class in the hierarchy of intellects.
	 *  It defines interface and simpliest possible intellect does nothing!
	 */
	public class BaseAI {

		/**
		 * Constructor
		 */
		public function BaseAI() {
			// constructor code
		}

		/**
		 * This method is caled each time ship wants intelect to control him.
		 * For each type of intellect, polymorphic verison is called.
		 */
		public function act( object: Ship ): void{
			// empty
		}

		/**
		 * Clone intellect.
		 */
		public function clone(): BaseAI{
			return new BaseAI();
		}

		/*
		 *  Basic operations
		 */

		/**
		 * Moving ship to exact destination with given engine intensity.
		 * @param sender Ship to move.
		 * @param position Position to reach.
		 * @param engineIntensity Engine intencity from 0(engine off) to 1(full engine).
		 */
		protected function moveToDestination( sender: Ship, position: Vector2d, engineIntensity: Number, reachRadius: Number ): Boolean{
			rotateTowardPosition(sender, position);
			sender.getEngine().enableForce(engineIntensity);

			if(sender.getPosition().getDistanceTo(position) < reachRadius)
			{
				sender.getEngine().disableForce();
				sender.getEngine().disableTorque();
				return true
			}

			return false;
		}

		/**
		 * Rotate the ship toward other object.
		 * @param sender Ship to move.
		 * @param position Position to reach.
		 */
		protected function rotateTowardPosition( sender: Ship, position: Vector2d): Boolean {
			// |A|*|B|*cos(alpha) = A*B
			var dir: Vector2d = Vector2d.fromPolar(1, sender.getRotationAngle());

			var distanceToTarget: Vector2d = position.sub(sender.getPosition());

			const cosAngle: Number = (distanceToTarget.scalarMul(dir))/distanceToTarget.norm;
			sender.getEngine().enableTorque(-cosAngle);

			return (cosAngle < Math.PI/32)
		}

		protected function attackObject( sender: Ship, target: SpaceObject ): Boolean {
			if(rotateTowardPosition(sender, target.getPosition()))
			{
				sender.shootWeapon(0); //TODO: now shoots the first added weapon, change to more sophisticated.
				return true;
			}
			return false;
		}

		protected function movingToObjectAndAttack( sender: Ship, target: SpaceObject ): Boolean {
			sender.getEngine().enableForce(1);
			return attackObject(sender, target);
		}

		public function addEnemy( object: SpaceObject ): void {
			// empty
		}

	}

}
