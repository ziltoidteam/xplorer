﻿package xplorer.AI {
    import xplorer.system.Vector2d;
    import xplorer.system.Random;
    import xplorer.game.objects.SpaceObject;
    import xplorer.game.requests.GameRequest;
    import xplorer.game.objects.Ship;

	/**
	 * This class creates intelect to simulate pirate.
	 * Pirate behaviour is very simple. It querries the game for all objects it can see and attacks 
	 * all of them. If no enemies left, querring game for more enemies.
	 */
	public class PirateAI extends BaseAI {
		var enemies: Vector.<SpaceObject> = new Vector.<SpaceObject>;
		var attackMode: Boolean = false;

		public function PirateAI() {
			// empty
		}

		/**
		 * Make a decision what to do now.
		 * Entrance point for AI algorithm.
		 */
		override public function act( object: Ship ): void{
			// If no engine, just idle.
			if(object.getEngine() == null) return;

			// If any enemies in the list, attack them, else be peacefull :)
			if(isAgressive())
			{
				agressiveBehaviour(object);
				// Querry game if current enemy is dead and if so, remove it from the list.
				object.dispatchEvent( new GameRequest(GameRequest.checkIfDead, getActiveEnemy(), this.removeActiveEnemy));
				// TODO: filter for same owner
			}
			else
				peacefulBehaviour(object);
		}

		/**
		 * In the case AI chooses to behave peacefully, it executes this.
		 * In the case of pirate, it just looks for more enemies.
		 * @param object object for AI to control.
		 */
		private function peacefulBehaviour( object: Ship ): void{
			// Search for more pray.
			object.dispatchEvent(new GameRequest(GameRequest.searchNearObjects, object, this.addEnemies));
			// TODO: if no enemies are found, we are entering in infinite loop querriing game for new enemies.
			//       it's a lot of calculation resouces. Add a waiting method.
			
			/* We need to filter all objects returned by game to those who need to be attacked and those
			 * who don't.
			 */
			sortEnemiesByPriority(object);
		}

		/**
		 * In the case AI chooses to behave agressively, it executes this.
		 * @param object object for AI to control.
		 */
		private function agressiveBehaviour( object: Ship ): void{
			var target = getActiveEnemy();

			var radiusVector = object.getPosition().sub(target.getPosition());

			if(radiusVector.norm < 5*object.getInteractionRadius())
				attackMode = false;
			if(radiusVector.norm > 10*object.getInteractionRadius())
				attackMode = true;

			if(attackMode)
				movingToObjectAndAttack(object,target);
			else
				moveToDestination(object, object.getPosition().add(radiusVector), 1, 0);
		}

		override public function clone(): BaseAI{
			return new PirateAI(); // TODO: arguments
		}

		/**
		 * This is a method to add one object as an enemy for this AI.
		 */
		override public function addEnemy( object: SpaceObject ): void{
			if(object != null)
				enemies.push(object);
		}

		/** 
		 * This is a method to add objects as an enemy for this AI.
		 */
		private function addEnemies(objects: Vector.<SpaceObject>): void{
			for each(var object in objects){
				addEnemy(object);
			}
		}

		/**
		 * Sorting filters all unwanted objects from enemies list and 
		 * sorts other so closer one gets more priority.
		 */
		private function sortEnemiesByPriority(object: SpaceObject): void{
			var filtered: Vector.<SpaceObject> = new Vector.<SpaceObject>;

			// Filter
			for each (var enemy in enemies){
				//can't attack youself or yor friendly object
				if(enemy.getOwner() == object.getOwner()) continue;

				filtered.push(enemy);
			}

			// Sort
			var compareFunc:Function = function(obj1:SpaceObject, obj2:SpaceObject):Number
			{
				const dist1: Number = object.getPosition().getDistanceTo(obj1.getPosition());
				const dist2: Number = object.getPosition().getDistanceTo(obj2.getPosition());

				if ( dist1 < dist2) return -1;
				else if (dist1 == dist2) return 0;
				else return 1;
			}

			filtered.sort(compareFunc);
			enemies = filtered;
		}

		/**
		 * Returns true if this AI do have at least one enemy.
		 */
		public function isAgressive(): Boolean{
			return (enemies.length > 0);
		}

		/**
		 * Returns current enemy who should be attacked.
		 */
		private function getActiveEnemy():SpaceObject{
			if(!isAgressive())
				return null;
			return enemies[0];
		}

		/**
		 * Remove current enemy from the list, probably it has been dead.
		 */
		private function removeActiveEnemy(): void{
			if(!isAgressive())
				return;

			enemies.splice(0,1);
		}

	}

}
