﻿package xplorer.AI {
    import xplorer.system.Vector2d;
	import xplorer.system.Random;
    import xplorer.game.objects.SpaceObject;
    import xplorer.game.requests.GameRequest;
    import xplorer.game.objects.Ship;

	/**
	 * This class creates intelect to simulate travelling.
	 * Object controlled by this intellect, travelling freelly until somebody attacks him,
	 * Then it begins to attack the attackers untill he wins or dies.
	 */
	public class TravellerAI extends BaseAI {
		// Algorithm uses those parameters at peaceful traveling.
		var checkpoint: Vector2d = null;
		var travelDirAngle:Number = Random.randomRange(0,2*Math.PI);
		// Algorithm uses those parameters for attacking other object.
		var attackMode: Boolean = false;

		var enemies: Vector.<SpaceObject> = new Vector.<SpaceObject>;

		public function TravellerAI() {
			// empty
		}

		/**
		 * Make a decision what to do now.
		 * Entrance point for AI algorithm.
		 */
		override public function act( object: Ship ): void{
			// If no engine, just idle.
			if(object.getEngine() == null) return;

			// If any enemies in the list, attack them, else just do travel.
			if(isAgressive())
			{
				agressiveBehaviour(object);
				// Querry game if current enemy is dead and if so, remove it from the list.
				object.dispatchEvent( new GameRequest(GameRequest.checkIfDead, getActiveEnemy(), this.removeActiveEnemy));
			}
			else
				peacefulBehaviour(object);
		}

		/**
		 * In the case AI chooses to behave peacefully, it executes this.
		 * @param object object for AI to control.
		 */
		private function peacefulBehaviour( object: Ship ): void{

			if(checkpoint == null)
			{
				this.chooseTravelCheckpoint(object);
			}
			else
			{
				if(moveToDestination( object, checkpoint, 0.5, object.getInteractionRadius() ))
					checkpoint = null;
			}
		}

		/**
		 * In the case AI chooses to behave agressively, it executes this.
		 * @param object object for AI to control.
		 */
		private function agressiveBehaviour( object: Ship ): void{
			var target = getActiveEnemy();

			var radiusVector = object.getPosition().sub(target.getPosition());

			if(radiusVector.norm < 5*object.getInteractionRadius())
				attackMode = false;
			if(radiusVector.norm > 10*object.getInteractionRadius())
				attackMode = true;

			if(attackMode)
				movingToObjectAndAttack(object,target);
			else
				moveToDestination(object, object.getPosition().add(radiusVector), 1, 0);
		}

		override public function clone(): BaseAI{
			return new TravellerAI(); // TODO: arguments
		}

		/**
		 * Choose next checkpoint for a travelling.
		 * We do make some small random correction to current travelling direction.
		 */
		private function chooseTravelCheckpoint( object: Ship ): void{
			travelDirAngle += Random.randomRange(-Math.PI/4, Math.PI/4);
			this.checkpoint = Vector2d.fromPolar(10 * object.getInteractionRadius(), travelDirAngle).add(object.getPosition());
		}

		/**
		 * This is a method to add some object as an enemy for this AI.
		 */
		override public function addEnemy( object: SpaceObject ): void{
			if(object != null)
				enemies.push(object);
		}

		/**
		 * Returns true if this AI do have at least one enemy.
		 */
		public function isAgressive(): Boolean{
			return (enemies.length > 0);
		}

		/**
		 * Returns current enemy who should be attacked.
		 */
		private function getActiveEnemy():SpaceObject{
			if(!isAgressive())
				return null;
			return enemies[0];
		}

		/**
		 * Remove current enemy from the list, probably it has been dead.
		 */
		private function removeActiveEnemy(): void{
			if(!isAgressive())
				return;

			enemies.splice(0,1);
		}

	}

}
