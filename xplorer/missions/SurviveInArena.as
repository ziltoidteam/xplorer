﻿package xplorer.missions {
    import flash.display.MovieClip;
    import flash.events.TimerEvent;
	import flash.events.Event;
    import flash.utils.Timer;

	import xplorer.ui.indicators.CheckBoxIndicator;
    import xplorer.ui.indicators.TextIndicator;

	import xplorer.game.requests.GameRequest;
	import xplorer.game.objects.Ship;
	import xplorer.game.Director;
	import xplorer.game.Game;

	import xplorer.system.Random;
	import xplorer.system.Vector2d;
	import xplorer.AI.PirateAI;

	public class SurviveInArena extends Mission {
        private var enemiesLeft: Number;
		private var enemiesTotal: Number;

		// UI
        var missionIndicator: CheckBoxIndicator;
        var enemiesIndicator: TextIndicator;


    	public function SurviveInArena(player: Ship, level: Number, enemies: Number) {
            this.enemiesLeft = enemies;
            this.enemiesTotal = enemies;

            super(player, level);

			placeEnemies(level);

            this.addEventListener( Event.ADDED_TO_STAGE, onAddedListener );
            this.addEventListener( Event.REMOVED_FROM_STAGE, onRemovedListener );
    	}

        override public function clone(): Mission {
            return new SurviveInArena(this.player, this.level, this.enemiesTotal);
        }

        override public function restart(): void {
            super.restart();

            this.enemiesLeft = this.enemiesTotal;
            this.placeEnemies(level);
        }

        override public function info(): String {
            var result: String = "";

            result += "[Survive in arena]\n";
            result += "Difficulty: " + String(int(this.level)) + "/100\n";
            result += "Enemies in arena: " + String(int(this.enemiesTotal)) + "\n";

            return result;
        }

		protected function onAddedListener(e: Event) {
            this.addEventListener( GameRequest.removeObject, removeObjectListener );
        }

        protected function onRemovedListener(e: Event) {
            this.removeEventListener( GameRequest.removeObject, removeObjectListener );
        }

		private function placeEnemies(level: Number): void{
			for( var i: int = 1; i <= getEnemiesTotal(); ++i){
			    var object: Ship = Director.generateShip(level);

			    var position: Vector2d = Vector2d.fromPolar(Random.randomRange(5*player.getInteractionRadius(),Game.maxSightRadius),
														    Random.randomRange(-Math.PI, +Math.PI));
			    position.add(this.player.getPosition());

				var angle: Number = Random.randomRange(-Math.PI, +Math.PI);

				object.setPosition(position);
				object.setRotationAngle(angle);
			    object.setIntelegence(new PirateAI());

				this.game.connect(object);
			}
		}

		private function getEnemiesLeft(): Number{
			return this.enemiesLeft;
		}

		private function getEnemiesTotal(): Number{
			return this.enemiesTotal;
		}

		private function removeObjectListener(r: GameRequest){
			if(!(r.getCaller() is Ship)) return;
			if( r.getCaller() == this.player) return;
			this.enemiesLeft --;
			updateIndicators();
		}

		override protected function initMissionUI(): void{
            this.missionIndicator = new CheckBoxIndicator("");
            this.enemiesIndicator = new TextIndicator("");
			updateIndicators();
		}

		private function updateIndicators(): void{
			this.missionIndicator.setValue("Survive the battle with " + this.getEnemiesTotal() + " enemies");
			this.enemiesIndicator.setValue("Deaths left: " + this.getEnemiesLeft());
		}

        override protected function getMissionUI(): MovieClip {
            var ui: MovieClip = new MovieClip();
            ui.addChild(missionIndicator);
            ui.addChild(enemiesIndicator);
			missionIndicator.x = 0
            missionIndicator.y += 0;
            enemiesIndicator.x = 0;
            enemiesIndicator.y += 30;

            return ui;
        }

        override protected function isComplete(): Boolean {
            return (this.getEnemiesLeft() <= 0);
        }

    }
}
