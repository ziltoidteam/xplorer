﻿package xplorer.missions {
    import flash.display.MovieClip;
    import xplorer.ui.indicators.CheckBoxIndicator;
    import xplorer.ui.indicators.TimeIndicator;
    import flash.events.TimerEvent;
	import flash.events.Event;
    import flash.utils.Timer;
    import xplorer.game.objects.Ship;

	public class SurviveInTime extends Mission {
        private var counter: Timer = new Timer(1000);
        private var timeLeft: Number;
		private var timeTotal: Number;

		// UI
        var missionIndicator: CheckBoxIndicator;
        var timeIndicator: TimeIndicator;


    	public function SurviveInTime(player: Ship, level: Number, missionTime: Number) {
            this.timeTotal = missionTime;
            this.timeLeft = missionTime;

            super(player, level);

            this.addEventListener( Event.ADDED_TO_STAGE, onAddedListener );
            this.addEventListener( Event.REMOVED_FROM_STAGE, onRemovedListener );
            counter.addEventListener(TimerEvent.TIMER, onCounter);
    	}

        override public function info(): String {
            var result: String = "";

            result += "[Survive for time]\n";
            result += "Difficulty: " + String(int(this.level)) + "/100\n";
            result += "Time to survive: " + String(int(this.timeTotal)) + " sec.\n";

            return result;
        }

        override public function clone(): Mission {
            return new SurviveInTime(this.player, this.level, this.timeTotal);
        }

        override public function restart(): void {
            super.restart();

            this.timeLeft = this.timeTotal;
        }

		protected function onAddedListener(e: Event) {
            counter.start();
        }

        protected function onRemovedListener(e: Event) {
            counter.stop();
        }
		private function getTimeLeft(): Number{
			return this.timeLeft;
		}

		private function getTimeTotal(): Number{
			return this.timeTotal;
		}

		override protected function initMissionUI(): void{
            this.missionIndicator = new CheckBoxIndicator("Survive for " + this.getTimeTotal() + " seconds");
            this.timeIndicator = new TimeIndicator("");
            this.timeIndicator.setTime(getTimeLeft());
		}

        override protected function getMissionUI(): MovieClip {
            var ui: MovieClip = new MovieClip();
            ui.addChild(missionIndicator);
            ui.addChild(timeIndicator);
            missionIndicator.y += 0;
            timeIndicator.y += 30;
            timeIndicator.x = missionIndicator.x;
            return ui;
        }

        override protected function isComplete(): Boolean {
            return (this.getTimeLeft() <= 0);
        }

        private function onCounter(e: TimerEvent) {
            if (this.getTimeLeft() > 0) this.timeLeft--;
			this.timeIndicator.setTime(this.getTimeLeft());
            if (this.getTimeLeft() <= 0) {
                missionIndicator.setChecked(true);
			}
        }
    }
}
