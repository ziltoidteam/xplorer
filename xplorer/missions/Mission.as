﻿package xplorer.missions {
    import flash.display.MovieClip;
    import flash.events.Event;
    import flash.events.TimerEvent;
    import flash.utils.Timer;
    import xplorer.system.Vector2d;
    import xplorer.game.Game;
    import xplorer.game.Director;
    import xplorer.game.objects.Ship;
	import xplorer.game.requests.WindowEvent;

	/**
	 * Base class for all missions in the game.
	 * Missions set goals for user to reach and control
	 * their fullfilness.
	 */
	public class Mission extends MovieClip {
	   // Ship for mission to control. It's not have to be player ship, hovewer it what we mean.
	   public var player: Ship;

	   // Mission creates game object will be stored here.
        public var game: Game;
        protected var level: Number;

        private var checkTimer: Timer = new Timer(1000);

        private const uiPosition: Vector2d = new Vector2d(100, 30);

        /**
         * Constructor gets plaer ship, which can go from one level to another, parameter for 
         * level hardness and initialzes all structures for mission to start. 
         * It creates Game and Director objects.
         * All inherited classes should call this super constructor directly.
         * @param player Ship for player control.
         * @param level Number from 0 to 100 for level hardness.
         */
    	public function Mission(player: Ship, level: Number) {
            this.player = player;
            this.level = level;

            var minLevel = Math.max(0, level - 10);
            var maxLevel = Math.min(100, level + 10);
            const director: Director = new Director(10, minLevel, maxLevel);

            this.game = new Game(this.player, director);
            this.addChild(game);

            this.initMissionUI();
            this.addMissionUI();

            this.addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
            this.addEventListener( Event.REMOVED_FROM_STAGE, onRemovedFromStage );
    	}

        public function restart(): void {
            this.player.reset();
            this.game.restart();
        }

    	/**
    	 * This virtual method should return if all goals for mission are reached or not.
    	 */
        protected function isComplete(): Boolean {
            // virtual
            return false;
        }

        public function clone(): Mission {
            // virtual
            return null;
        }

        public function info(): String {
            var result: String = "";

            result += "[Abstract Mission]\n";
            result += "Player: " + String(this.player) + "\n";
            result += "Game: " + String(this.game) + "\n";
            result += "Level: " + String(this.level) + "\n";

            return result;
        }

        protected function isFailed(): Boolean {
            // semi-virtual
            return (player.getHealthPoints() <= 0);
        }
		
		/**
		 * This method called each time player completes the mission.
		 * It can be overrided in each inherited class to customize mission completness.
		 */
		protected function onComplete(): void{
			// virtual
		}

		/**
		 * This method called each time player failes the mission.
		 * It can be overrided in each inherited class to customize mission completness.
		 */
		protected function onFail(): void{
			trace(" Game over. You loose");
		}

	/**
	 * 
	 */
        protected function addMissionUI(): void {
            var ui: MovieClip = this.getMissionUI();

            if (ui != null) {
                ui.x = uiPosition.x;
                ui.y = uiPosition.y;
                this.addChild(ui);
            }
        }

        protected function getMissionUI(): MovieClip {
            // virtual
            return null;
        }
		
		/**
		 * Initializes user interface 
		 */
		protected function initMissionUI(): void{
			// virual
		}

        private function checkTimerListener(e: Event) {
            // Check mission state, win has better priority than fail.
            if (this.isComplete()){
                onComplete();
				this.dispatchEvent(new WindowEvent(WindowEvent.successInMission));
			}
            else if (this.isFailed()){
                onFail();
			    this.dispatchEvent(new WindowEvent(WindowEvent.failInMission));
			}
        }

        private function onAddedToStage(e: Event) {
            checkTimer.addEventListener(TimerEvent.TIMER, checkTimerListener);
            checkTimer.start();
        }

        private function onRemovedFromStage(e: Event) {
            checkTimer.removeEventListener(TimerEvent.TIMER, checkTimerListener);
            checkTimer.stop();
        }
	}
}
