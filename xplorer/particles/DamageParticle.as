﻿package xplorer.particles {
	import flash.display.MovieClip;
	import flash.events.*;
    import xplorer.system.Vector2d;


	/**
	 * Class to handle anmation for damage scoring animation.
	 */
	public class DamageParticle extends MovieClip {
		var fadeSpeed: Number;
		var speed: Vector2d;

		/**
		 * Constructs an object
		 * Add it to scene to begin animation.
		 * @param value Value to animate.
		 * @param size Font size for value.1 is the default
		 * @param speed Direction and velocity of animation.
		 * @param fadeSpeed Percents in second, speed of transparensy effect for animation to completely disapear.
		 */
		public function DamageParticle(value: Number = 0, size: Number = 1, speed: Vector2d = null, fadeSpeed: Number = 30) {
			this.setValue(value);
			this.fadeSpeed = fadeSpeed;
			this.speed = speed;
			this.scaleX = this.scaleY = size;

			this.addEventListener( Event.ADDED_TO_STAGE, onAddedListener );
			this.addEventListener( Event.REMOVED_FROM_STAGE, onRemovedListener );
		}

		public function setValue(value: Number) {
			this.score.text = String(int(value));
		}

        private function onEnterFrame(e: Event) {
            this.alpha -= this.fadeSpeed / 100 / stage.frameRate;
			if (speed != null) {
				this.x += speed.x; //TODO: delta t needed?
				this.y += speed.y;
			}

            if (this.alpha <= 0) {
                this.removeEventListener( Event.ENTER_FRAME, onEnterFrame );
				this.parent.removeChild(this);
            }
        }

		private function onAddedListener(e: Event) {
            this.addEventListener( Event.ENTER_FRAME, onEnterFrame );
		}

		private function onRemovedListener(e: Event) {
            this.removeEventListener( Event.ENTER_FRAME, onEnterFrame );
		}
	}

}
