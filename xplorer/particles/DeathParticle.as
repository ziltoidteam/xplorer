﻿package xplorer.particles {
	import flash.display.MovieClip;
    import flash.events.Event;
    import xplorer.system.Vector2d;

    public class DeathParticle extends MovieClip {
        private const fadeSpeed: Number = 10; // percents per second

        public function DeathParticle(position: Vector2d) {
            this.x = position.x;
            this.y = position.y;

			this.addEventListener( Event.ADDED_TO_STAGE, onAddedListener );
			this.addEventListener( Event.REMOVED_FROM_STAGE, onRemovedListener );
        }

        private function onEnterFrame(e: Event) {
            this.alpha -= this.fadeSpeed / 100 / stage.frameRate;

            if (this.alpha <= 0) {
                this.removeEventListener( Event.ENTER_FRAME, onEnterFrame );
				this.parent.removeChild(this);
            }
        }

		private function onAddedListener(e: Event) {
            this.addEventListener( Event.ENTER_FRAME, onEnterFrame );
		}

		private function onRemovedListener(e: Event) {
            this.removeEventListener( Event.ENTER_FRAME, onEnterFrame );
		}
    }

}
