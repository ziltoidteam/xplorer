﻿package xplorer.system {
    public class Random {
        public static function random(): Number {
            return Math.random();
        }

        public static function randomTo( max: Number ): Number {
            return Random.random() * max;
        }

        public static function randomRange( min: Number, max: Number ): Number {
            if (max < min){
				var temp: Number = min;
				min = max;
				max = min;
			}

            return min + Random.randomTo(max - min);
        }
    }
}
