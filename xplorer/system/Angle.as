﻿package xplorer.system {
	public class Angle {
		public static function radiansToDegrees( angleInRadians: Number ): Number {
			return angleInRadians * 180 / Math.PI;
		}

		public static function degreesToRadians( angleInDegrees: Number ): Number {
			return angleInDegrees * Math.PI / 180;
		}
	}
}
