﻿package xplorer.system {
	public class Html {
        public static function fold(tag: String, text: String, arguments: String = null): String {
            if (arguments)
                return "<" + tag + " " + arguments + ">" + text + "</" + tag + ">";
            else
                return "<" + tag + ">" + text + "</" + tag + ">";
        }

		public static function colorize(text: String, color: String): String {
            return fold("font", text, "color=\"" + color + "\"");
		}

        public static function bold(text: String): String {
            return fold("b", text);
        }

        public static function underline(text: String): String {
            return fold("u", text);
        }

        public static function italic(text: String): String {
            return fold("i", text);
        }
	}
}
