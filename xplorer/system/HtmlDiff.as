﻿package xplorer.system {
	public class HtmlDiff {
        public static function good(text: Number): String {
            return Html.colorize(String(text), "#00FF00");
        }

        public static function bad(text: Number): String {
            return Html.colorize(String(text), "#FF0000");
        }

        public static function compareInt(newValue: int, oldValue: int): String {
            return compare(newValue, oldValue);
        }

        public static function inverseCompareInt(newValue: int, oldValue: int): String {
            return inverseCompare(newValue, oldValue);
        }

        public static function compare(newValue: Number, oldValue: Number): String {
            var result: String = String(newValue);

            if (newValue > oldValue) {
                result = good(newValue);
            } else if (oldValue > newValue) {
                result = bad(newValue);
            }

            result += ", old(" + String(oldValue) + ")";

            return result;
        }

        public static function inverseCompare(newValue: Number, oldValue: Number): String {
            var result: String = String(newValue);

            if (newValue < oldValue) {
                result = good(newValue);
            } else if (oldValue < newValue) {
                result = bad(newValue);
            }

            result += ", old(" + String(oldValue) + ")";

            return result;
        }
	}
}
