﻿package xplorer.system {
	import xplorer.system.Random;

	public class Range {
		var min: Number = 0;
		var max: Number = 0;
		var value: Number = 0;

		public function Range(min: Number, max: Number) {
			if( max < min )
				throw "eror in Range: max is bigger than min";
			this.min = min;
			this.max = max;

			this.value = 0;
		}

		public function getLevel(): Number{
			const delta: Number = max - min;
			return Math.floor((value-min) / delta * 100);
		}

		public function getValue(): Number{
			return this.value;
		}

		public function generate(level: Number): Number {
			if(level < 0 || level > 100)
				throw "error in Range: Percent must be in range from 0 to 100";

			const delta: Number = max - min;
			const result: Number = min + (delta/100)*level;

			this.value = result;
			return result;
		}

		public function random(level: Number): Number{
			const min: Number = Math.round(level/1.5);
			return generate(Random.randomRange(min, level));
		}

		public function randomInverse(level: Number): Number{
			this.value = max - random(level) + min;
			return this.value;
		}

	}

}
