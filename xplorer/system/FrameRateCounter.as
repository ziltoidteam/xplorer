﻿package xplorer.system {
	import flash.display.Stage;
	import flash.events.Event;
	import flash.utils.getTimer;

    /**
     * This static object calculates actual framerate of the rendering.
     * TODO: ordinary class, no constant model?
     */
    public class FrameRateCounter {
        private static var stage: Stage;
        private static var currentTime: Number;
        private static var m_maxStatisticCount: Number;
        private static var times: Vector.<Number> = new Vector.<Number>();

        /**
         * Initializes framerate counter.
         */
        public static function init( stage: Stage ) {
            FrameRateCounter.stage = stage;
            maxStatisticCount = stage.frameRate / 2;
            stage.addEventListener( Event.ENTER_FRAME, onEnterFrameListener );
        }

        public static function set maxStatisticCount(value: Number): void {
            m_maxStatisticCount = value;
        }

        public static function get maxStatisticCount(): Number {
            return m_maxStatisticCount;
        }

        /**
         * Return current millisec per frame parameter.
         */
        public static function getFrameTime(): Number {
            return FrameRateCounter.currentTime;
        }

        /**
         * Return current framerate, frames per sec.
         */
        public static function getFrameRate(): Number {
            return Math.round(1000 / FrameRateCounter.currentTime);
        }

        public static function getStatisticFrameRate(): Number {
            if (times.length == 0) return getFrameRate();

            var summaryTime: Number = 0;
            for each (var aTime: Number in times) {
                if (!isNaN(aTime)) summaryTime += aTime;
            }

            var averageTime: Number = summaryTime / times.length;

            return Math.round(1000 / averageTime);
        }

        private static function updateStatistic( newTime: Number ): void {
            times.unshift(newTime);
            if (times.length > maxStatisticCount) {
                times.pop();
            }
        }

        /**
         * On entering frame listener, updating framerate status.
         */
    	private static var lastTime: Number;
        private static function onEnterFrameListener(e: Event) {
            currentTime = getTimer() - lastTime;
            lastTime = getTimer();

            updateStatistic(currentTime);
        }
    }
}

