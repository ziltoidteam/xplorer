﻿package xplorer.system {
	public class Vector2d {
		private var m_x;
		private var m_y;

		public function Vector2d( newX: Number, newY: Number ) {
			this.m_x = newX;
            this.m_y = newY;
		}

        public function get x(): Number {
            return this.m_x;
        }

        public function get y(): Number {
            return this.m_y;
        }

        public function get angle(): Number {
            return Math.atan2( this.y, this.x );
        }

        public function get norm(): Number {
            return Math.sqrt( Math.pow(this.x, 2) + Math.pow(this.y, 2) );
        }

        public function getDistanceTo(other: Vector2d): Number{
            return (new Vector2d(other.x - this.x, other.y - this.y)).norm;
        }

        public function getManhattenDistanceTo(other: Vector2d): Number{
            return (Math.abs(other.x - this.x)+ Math.abs(other.y - this.y));
        }

        public function getAngleTo(other: Vector2d): Number{
            return Math.acos(this.scalarMul(other) / ( this.norm * other.norm));
        }

        public function normalize(): Vector2d {
			if( this.norm != 0 )
                return this.mul(1/this.norm);
            return new Vector2d(0,0);
        }

        public function add( delta: Vector2d ): Vector2d {
            return new Vector2d( this.x + delta.x, this.y + delta.y );
        }

        public function sub( delta: Vector2d ): Vector2d {
            return new Vector2d( this.x - delta.x, this.y - delta.y );
        }

        public function scalarMul( other: Vector2d ): Number {
            return this.x*other.x + this.y*other.y;
        }

        public function mul( scalar: Number ): Vector2d {
            return new Vector2d( this.x * scalar, this.y * scalar );
        }

        public function rotate( angle: Number ): Vector2d {
            return new Vector2d(
                this.x*Math.cos(angle) - this.y*Math.sin(angle)
                , this.x*Math.sin(angle) + this.y*Math.cos(angle)
            );
        }

        public function toString(): String {
            return "(" + String(this.x) + ", " + String(this.y) + ")";
        }

        public static function fromPolar( norm: Number, angle: Number ): Vector2d {
            return new Vector2d( norm * Math.cos(angle), norm * Math.sin(angle) );
        }
	}

}
