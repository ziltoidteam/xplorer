package xplorer.system {
    public class Debug {
        private static var debug_mode = "off"; // "on" or "off"

        public static function out( ...something ): void {
            if (Debug.debug_mode == "on") {
                var text: String = "";
                for each (var some in something) {
                    text += some;
                }

                trace( text );
            }
        }

        public static function set_debug_mode( newMode: String ): void {
            Debug.debug_mode = newMode;
        }
    }
}
