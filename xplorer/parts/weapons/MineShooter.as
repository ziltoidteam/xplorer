﻿package xplorer.parts.weapons {
    import xplorer.parts.ShipPart;
    import xplorer.system.Vector2d;
    import xplorer.system.Random;
    import xplorer.game.objects.SpaceObject;
    import xplorer.game.requests.GameRequest;
    import xplorer.parts.weapons.shells.Mine;

	public class MineShooter extends WeaponObject {
		var mine: Mine;
		var quantity: Number;
        var maxQuantity: Number;
        var speed: Number;

		public function MineShooter( respawnTime: Number, mine: Mine, quantity: Number, speed: Number) {
			super(respawnTime);
			this.mine = mine.clone() as Mine;
			this.quantity = int(quantity);
            this.maxQuantity = int(quantity);
            this.speed = speed;
		}

		override public function clone(): ShipPart {
			return new MineShooter(getRespawnTime(), mine, getBulletsLeft(), speed);
		}

		public function getBulletsLeft(): Number{
			return quantity;
		}

		override protected function shootNow(shooter: SpaceObject): Boolean{
			// No rockets left check.
			if(quantity <= 0)
				return false;

			var shootMine: Mine = this.mine.clone() as Mine;
			// Update the mine position
			shootMine.setPosition(shooter.getPosition());
			shootMine.setRotationAngle(shooter.getRotationAngle());
			shootMine.setSender(shooter);

            var angle = shooter.getRotationAngle() + Math.PI / 2 + Random.randomRange(-0.2, 0.2);
            var vel: Vector2d = shooter.getVelocity().add(Vector2d.fromPolar(this.speed, angle));

            shootMine.setVelocity(vel.add(shooter.getVelocity()));
            shootMine.setAngularVelocity(Random.randomRange(-30,30));
			shooter.dispatchEvent(new GameRequest(GameRequest.createObject, shootMine));

			// Update launcher status after shooting.
			quantity--;

			return true;
		}

        override public function setQuantity(value: Number) {
            this.quantity = value;
        }

        override public function getQuantity(): Number {
            return this.quantity;
        }

        override public function getMaxQuantity(): Number {
            return this.maxQuantity;
        }

        /**
	 * This method returns short information about weapon as a string.
	 * Only information relevant to user-side included.
	 */
        override public function info(): String{
        	var result: String = new String();
        	result += "Mine shooter:\n";
        	result += "Reload time " + this.getRespawnTime() + "\n";
        	result += "Shoots: " + this.getQuantity() + " / " + this.getMaxQuantity() + "\n";
        	result += "Init. velocity: " + this.speed + "\n";
        	result += "Initial speed: " + this.speed + "\n";
        	if(mine != null){
        		result += "Mine mass: " + mine.getMass() + "\n";
        		result += "Mine damage: " + mine.getDamageValue() + "\n";
        	}

        	return result;
        }
	}

}
