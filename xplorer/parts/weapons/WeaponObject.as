﻿package xplorer.parts.weapons {
    import xplorer.parts.ShipPart;
    import xplorer.game.objects.SpaceObject;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
    import xplorer.game.objects.Ship;

	/**
	 * Base class for all weapon devices in the game, like rocket launchers, lasers, etc.
	 * The basic functionality it provides is possibility to shoot only after given respawn time.
	 */
	public class WeaponObject implements ShipPart {
		var respawnTimer: Timer = null;

		public function WeaponObject( respawnTime: Number) {
			setRespawnTime(respawnTime);
		}

		/**
		 * Clones the weapon.
		 * Be aware, that cloned timer initializes unstarted, even if this weapon's timer is running.
		 */
		public function clone(): ShipPart {
			return new WeaponObject(getRespawnTime());
		}

		/**
		 * Set respawn time for a weapon.
		 * @param respawnTime Sets the respawn time interval for a weapon. 0 menas no respawn time.
		 */
		public function setRespawnTime(respawnTime: Number):void{
			this.respawnTimer = new Timer(respawnTime,1);
		}

		/**
		 * Get respawn time for a weapon.
		 */
		public function getRespawnTime() : Number{
			return this.respawnTimer.delay;
		}

		/**
		 * Returns true if weapon can shoot this moment.
		 */
		public function isReadyToShoot(): Boolean{
			return !respawnTimer.running;
		}

		/**
		 * This is the common method to shoot for any weapon.
		 * It prevents shooting if respawn time not over and weapon still loads.
		 * It initializes the timer for new respawn time and actually shoot.
		 * Returns true if rocket has been shoot, no matter if it has been targetted or not.
		 */
		public function shoot(shooter: SpaceObject): Boolean{
			if(!isReadyToShoot())
				return false;

			respawnTimer.reset();
			respawnTimer.addEventListener(TimerEvent.TIMER_COMPLETE, respawnEvent);
			respawnTimer.start();

			return shootNow(shooter);
		}

        public function setQuantity(value: Number) {
            // virtual
            return;
        }

        public function getQuantity(): Number {
            // virtual
            return 0;
        }

        public function getMaxQuantity(): Number {
            // virtual
            return 0;
        }

		/**
		 * Method to override for every weapon. Makes a shoot.
		 */
		protected function shootNow(shooter: SpaceObject): Boolean{
			return false;
		}

		/**
		 * After a time waiting for a respawn, restore the ability to shoot.
		 */
		private function respawnEvent(e: TimerEvent):void{
			respawnTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, respawnEvent);
			respawnTimer.stop();
		}

		public function info(): String{
			return String(this);
		}

        public function setup(ship: Ship): void {
            ship.setWeapon(this);
        }

        public function setupable(ship: Ship): Boolean {
            return true;
        }
	}

}
