﻿package xplorer.parts.weapons.shells {
    import xplorer.parts.engines.Engine;
    import xplorer.system.Vector2d;
	import xplorer.system.Random;
    import xplorer.animation.Boom;
    import xplorer.traces.*;
    import xplorer.game.objects.SpaceObject;
    import xplorer.game.objects.FlyingObject;
    import xplorer.game.Lut;

	public class Rocket extends FlyingObject {
		private var ROCKET_IRADIUS:Number = 5;

		var sender: SpaceObject = null;
		var target: SpaceObject = null;
        var trace: Trace = null;

		/**
		 * Constructor
		 */
		public function Rocket(objectMass: Number, objectMomentOfInertia: Number,
							   position: Vector2d, rotationAngle: Number, velocity: Vector2d, angularVelocity: Number,
							   externalForce: Vector2d, externalTorque: Number,
							   damageValue: Number, healthPoints: Number,  maxHealthPoints: Number, lut: Lut,
							   engine: Engine, sender: SpaceObject, target: SpaceObject ) {
			super(objectMass, objectMomentOfInertia, position, rotationAngle, velocity, angularVelocity,
				  externalForce, externalTorque, ROCKET_IRADIUS, damageValue, healthPoints,  maxHealthPoints, lut, engine);
			this.setSender(sender);
			this.setTarget(target);

            this.trace = new Trace(20);
            this.addChild(this.trace);
		}

		/**
		 * Cloning function
		 */
		override public function clone(): SpaceObject{
				return new Rocket(getMass(), getMomentOfInertia(), getPosition(), getRotationAngle(),
								  getVelocity(), getAngularVelocity(), getExternalForce(), getExternalTorque(),
								  getDamageValue(), getHealthPoints(), getMaxHealthPoints(), getLut(), getEngine(),
								  getSender(), getTarget());

		}

		override protected function applyIntelegence(): void {
			// If no engine, just idle
			if(this.getEngine() == null) return;

			// If no fuel, self terminate
			if(this.getEngine().getFuel() == 0)
				applyCollision(null);

			// if no target to lock on, just move forward till death
			if(target == null || target.parent == null)
			{
				this.getEngine().enableForce(1);;
				this.getEngine().disableTorque();
				return ;
			}

			// |A|*|B|*cos(alpha) = A*B
			var dir: Vector2d = Vector2d.fromPolar(1,this.getRotationAngle());

			var distanceToTarget: Vector2d = target.getPosition().sub(this.getPosition());

			var cosAngle: Number = (distanceToTarget.scalarMul(dir))/distanceToTarget.norm;

			this.getEngine().enableTorque(-cosAngle);
			this.getEngine().enableForce(1);
		}

		public function chooseTarget( objects: Vector.<SpaceObject> ): void{

			var target: SpaceObject = null;
			var minDistance: Number = 0;

			for each( var object in objects){
				if( object == this.getSender()) continue;

				var toTarget: Vector2d = object.getPosition().sub(this.getPosition());

				var angleDiff: Number = Math.abs(toTarget.angle - (this.getRotationAngle() + Math.PI/2));
				angleDiff = angleDiff % (2*Math.PI);

				var distance: Number = toTarget.norm;
				if( (angleDiff < Math.PI/4 || angleDiff > (2*Math.PI - Math.PI/4)) &&
					(target == null || distance < minDistance)){
					minDistance = distance;
					target = object;
				}
			}

			setTarget(target);
		}

		public function setTarget(target: SpaceObject){
			this.target = target;
		}

		public function getTarget(): SpaceObject{
			return (this.target);
		}

		public function setSender(sender: SpaceObject){
			this.sender = sender;
		}

		public function getSender(): SpaceObject{
			return (this.sender);
		}

        override public function applyCollision( other: SpaceObject ): void {
			if(this.parent == null)
				return;

            this.setHealthPoints(0);
        }

        override protected function destruction(): void{
			trace.end();
			this.playBoomAnimation(30, 0.3);

			this.createShockWave(30,30);
        }

	/**
	 * Owner of the object is the object actually shoot this rocket.
	 */
	override public function getOwner(): SpaceObject{
			return getSender();
		}

	}

}
