﻿package xplorer.parts.weapons.shells {
    import xplorer.system.Vector2d;
	import xplorer.system.Random;
    import xplorer.animation.Boom;
    import xplorer.traces.*;
    import xplorer.game.objects.SpaceObject;
    import xplorer.game.Lut;

	public class Bullet extends SpaceObject {
		private var BULLET_IRADIUS:Number = 6;

		var sender: SpaceObject = null;
        var trace: TraceBasic = null;

		/**
		 * Constructor
		 */
		public function Bullet(objectMass: Number, objectMomentOfInertia: Number,
							   position: Vector2d, rotationAngle: Number, velocity: Vector2d, angularVelocity: Number,
							   externalForce: Vector2d, externalTorque: Number,
							   damageValue: Number, healthPoints: Number,  maxHealthPoints: Number, sender: SpaceObject ) {
			super(objectMass, objectMomentOfInertia, position, rotationAngle, velocity, angularVelocity,
				  externalForce, externalTorque, BULLET_IRADIUS, damageValue, healthPoints,  maxHealthPoints, new Lut());
			this.setSender(sender);

            this.trace = new CustomTrace(null, TraceEnd, null, 300, 20);
            this.addChild(this.trace);
		}

		/**
		 * Cloning function
		 */
		override public function clone(): SpaceObject{
				return new Bullet(getMass(), getMomentOfInertia(), getPosition(), getRotationAngle(),
								  getVelocity(), getAngularVelocity(), getExternalForce(), getExternalTorque(),
								  getDamageValue(), getHealthPoints(), getMaxHealthPoints(),
								  getSender());

		}

		public function setSender(sender: SpaceObject){
			this.sender = sender;
		}

		public function getSender(): SpaceObject{
			return (this.sender);
		}

        override public function applyCollision( other: SpaceObject ): void {
			if(this.parent == null)
				return;

            this.setHealthPoints(0);
        }

        override protected function destruction(): void{
            trace.end();

			this.playBoomAnimation(10, 0.1);
        }

	/**
	 * Owner of the object is the object actually shoot this Bullet.
	 */
	override public function getOwner(): SpaceObject{
			return getSender();
		}

	}

}
