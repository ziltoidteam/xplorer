﻿package xplorer.parts.weapons.shells {
    import xplorer.parts.engines.Engine;
    import xplorer.system.Vector2d;
	import xplorer.system.Random;
    import xplorer.animation.Boom;
    import xplorer.traces.*;
    import xplorer.game.objects.SpaceObject;
    import xplorer.game.objects.FlyingObject;
    import xplorer.game.Lut;

	public class Mine extends FlyingObject {
		private var MINE_IRADIUS:Number = 10;

		var sender: SpaceObject = null;
		var shockWavePeakDamage: Number = 0;;
		var shockWaveExpancionRadius: Number = 0;;

		/**
		 * Constructor
		 */
		public function Mine(objectMass: Number, objectMomentOfInertia: Number,
							   position: Vector2d, rotationAngle: Number, velocity: Vector2d, angularVelocity: Number,
							   externalForce: Vector2d, externalTorque: Number,
							   damageValue: Number, healthPoints: Number,  maxHealthPoints: Number, lut: Lut,
							   sender: SpaceObject, shockWavePeakDamage: Number, shockWaveExpancionRadius: Number  ) {
            var engine: Engine = new Engine(0, 0, 0, 0, 0.0005, 0.3, 0, 0);
			super(objectMass, objectMomentOfInertia, position, rotationAngle, velocity, angularVelocity,
				  externalForce, externalTorque, MINE_IRADIUS, damageValue, healthPoints,  maxHealthPoints, lut, engine);
			this.setSender(sender);
			this.shockWavePeakDamage = shockWavePeakDamage;
			this.shockWaveExpancionRadius = shockWaveExpancionRadius;
		}

		/**
		 * Cloning function
		 */
		override public function clone(): SpaceObject{
				return new Mine(getMass(), getMomentOfInertia(), getPosition(), getRotationAngle(),
								  getVelocity(), getAngularVelocity(), getExternalForce(), getExternalTorque(),
								  getDamageValue(), getHealthPoints(), getMaxHealthPoints(), getLut(),
								  getSender(), this.shockWavePeakDamage, this.shockWaveExpancionRadius);

		}

		public function setSender(sender: SpaceObject){
			this.sender = sender;
		}

		public function getSender(): SpaceObject{
			return (this.sender);
		}

        override public function applyCollision( other: SpaceObject ): void {
			if(this.parent == null)
				return;

            this.setHealthPoints(0);
        }

        override protected function destruction(): void{
			this.playBoomAnimation(30, 0.9);

			this.createShockWave(shockWavePeakDamage,shockWaveExpancionRadius);
        }

	/**
	 * Owner of the object is the object actually shoot this rocket.
	 */
	override public function getOwner(): SpaceObject{
			return getSender();
		}

	}

}
