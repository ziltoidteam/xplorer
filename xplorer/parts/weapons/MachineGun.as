﻿package xplorer.parts.weapons {
    import xplorer.parts.ShipPart;
    import xplorer.system.Random;
    import xplorer.system.Vector2d;
    import xplorer.game.objects.SpaceObject;
    import xplorer.game.requests.GameRequest;
    import xplorer.parts.weapons.shells.Bullet;

	public class MachineGun extends WeaponObject {
		var bullet: Bullet;
		var quantity: Number;
        var maxQuantity: Number;
        var speed: Number;

		public function MachineGun(respawnTime: Number, bullet: Bullet, quantity: Number, speed: Number) {
			super(respawnTime);
			this.bullet = bullet.clone() as Bullet;
			this.quantity = int(quantity);
            this.maxQuantity = int(quantity);
            this.speed = speed;
		}

		override public function clone(): ShipPart {
			return new MachineGun(getRespawnTime(), bullet, getBulletsLeft(), speed);
		}

		public function getBulletsLeft(): Number{
			return quantity;
		}

		override protected function shootNow(shooter: SpaceObject): Boolean{
			// No rockets left check.
			if(quantity <= 0)
				return false;

			var shootBullet: Bullet = this.bullet.clone() as Bullet;
			// Update the bullet position
			shootBullet.setPosition(shooter.getPosition());
			shootBullet.setVelocity(shooter.getVelocity());
			shootBullet.setRotationAngle(shooter.getRotationAngle());
			shootBullet.setSender(shooter);

            var angle = shooter.getRotationAngle() + Math.PI / 2 + Random.randomRange(-0.2, 0.2);
            var vel: Vector2d = shooter.getVelocity().add(Vector2d.fromPolar(this.speed, angle));

            shootBullet.setVelocity(vel);
			shooter.dispatchEvent(new GameRequest(GameRequest.createObject, shootBullet));

			// Update launcher status after shooting.
			quantity--;

			return true;
		}

        override public function setQuantity(value: Number) {
            this.quantity = value;
        }

        override public function getQuantity(): Number {
            return this.quantity;
        }

        override public function getMaxQuantity(): Number {
            return this.maxQuantity;
        }

        /**
	 * This method returns short information about weapon as a string.
	 * Only information relevant to user-side included.
	 */
        override public function info(): String{
        	var result: String = new String();
        	result += "Machine gun:\n";
        	result += "Reload time " + this.getRespawnTime() + "\n";
        	result += "Shoots: " + this.getBulletsLeft() + " / " + this.getMaxQuantity() + "\n";
        	result += "Init. velocity: " + this.speed + "\n";
        	if(bullet != null){
        		result += "Bullet mass: " + bullet.getMass() + "\n";
        		result += "Bullet damage: " + bullet.getDamageValue() + "\n";
        	}

        	return result;
        }

	}

}
