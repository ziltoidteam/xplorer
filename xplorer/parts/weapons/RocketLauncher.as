﻿package xplorer.parts.weapons {
    import xplorer.parts.ShipPart;
    import xplorer.game.objects.SpaceObject;
    import xplorer.game.requests.GameRequest;
    import xplorer.parts.weapons.shells.Rocket;

	public class RocketLauncher extends WeaponObject {
		var rocket: Rocket;
		var quantity: Number;
        var maxQuantity: Number;

		/**
		 * Initializes rocket launcher with rocket type it shoots and number of those rockets.
		 */
		public function RocketLauncher( respawnTime: Number, rocket: Rocket, quantity: Number) {
			super(respawnTime);
			this.rocket = rocket.clone() as Rocket;
			this.quantity = int(quantity);
            this.maxQuantity = int(quantity);
		}

		/**
		 * Clones the weapon.
		 * Be aware, that cloned timer initializes unstarted, even if this weapon's timer is running.
		 */
		override public function clone(): ShipPart {
			return new RocketLauncher(getRespawnTime(), rocket, getRocketsLeft());
		}

		/**
		 * Returns number of left rockets in the launcher.
		 */
		public function getRocketsLeft(): Number{
			return quantity;
		}

		/**
		 * Shoots the rocket if there is still rockets in the launcher.
		 * Returns true if rocket has been shoot, no matter if it has been targetted or not.
		 * @param shooter The object who shoots the rocket, basicly ship.
		 */
		override protected function shootNow(shooter: SpaceObject): Boolean{
			// No rockets left check.
			if(quantity <= 0)
				return false;

			var shootRocket: Rocket = this.rocket.clone() as Rocket;
			// Update the rocket position
			shootRocket.setPosition(shooter.getPosition());
			shootRocket.setVelocity(shooter.getVelocity());
			shootRocket.setRotationAngle(shooter.getRotationAngle());
			shootRocket.setSender(shooter);
			// Shooter object need to be added to scene hierarchy.
			// So we making request for a game to add it and locate a target for it.
			shooter.dispatchEvent(new GameRequest(GameRequest.searchNearObjects, shootRocket, shootRocket.chooseTarget));
			shooter.dispatchEvent(new GameRequest(GameRequest.createObject, shootRocket));

			// Update launcher status after shooting.
			quantity--;

			return true;
		}

        override public function setQuantity(value: Number) {
            this.quantity = value;
        }

        override public function getQuantity(): Number {
            return this.quantity;
        }

        override public function getMaxQuantity(): Number {
            return this.maxQuantity;
        }

        /**
	 * This method returns short information about weapon as a string.
	 * Only information relevant to user-side included.
	 */
        override public function info(): String{
        	var result: String = new String();
        	result += "Rocket launcher:\n";
        	result += "Reload time " + this.getRespawnTime() + "\n";
        	result += "Shoots: " + this.getQuantity() + " / " + this.getMaxQuantity() + "\n";
        	if(rocket != null){
        		result += "Rocket mass: " + rocket.getMass() + "\n";
        		result += "Rocket damage: " + rocket.getDamageValue() + "\n";
        	}

        	return result;
        }

	}

}
