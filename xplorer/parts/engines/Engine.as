﻿package xplorer.parts.engines {
	import xplorer.parts.ShipPart;
    import xplorer.game.objects.Ship;

	/**
	 * Class for engine model in the game.
	 */
	public class Engine implements ShipPart {

		private var force: Number;
		private var torque: Number;

		private var forceMax: Number;
		private var torqueMax: Number;

		private var forceFriction: Number;
		private var torqueFriction: Number;

		/*
		 * Fuel is a parameter at seconds, time of engine working at full power.
		 */
		private var fuel: Number ;
		private var fuelMax: Number;

		/**
		 *  Initializes the engine.
		 */
		public function Engine(force: Number, forceMax : Number, torque: Number, torqueMax: Number, forceFriction: Number, torqueFriction: Number, fuel: Number, fuelMax: Number) {
			this.force = force;
			this.forceMax = forceMax;
			this.torque = torque;
			this.torqueMax = torqueMax;

			this.forceFriction = forceFriction;
			this.torqueFriction = torqueFriction;

			this.fuelMax = fuelMax;
			this.fuel = fuel;
		}

		/**
		 * Clones the engine.
		 */
		public function clone(): ShipPart {
			  return new Engine( this.getForce(), this.forceMax, this.getTorque(), this.torqueMax,
					     this.forceFriction, this.torqueFriction, this.getFuel(), this.fuelMax );
		}

		/**
		 *  Turn engine on for linear motion to given intensity.
		 *  @param coefficient the intensity of engine, where 0 is engine is off and 1 is engine at full power.
		 */
		public function enableForce(coefficient: Number): void {
			var newForce = Math.min(forceMax, forceMax*coefficient);
			newForce = Math.max(newForce,0);

			this.force = newForce;
		}

		/**
		 *  Turn engine off for linear motion. Equivalent to enableForce(0).
		 */
		public function disableForce(): void{
			this.force = 0;
		}

		/**
		 *  Returns linear force of engine. If there is no fuel, it returns 0, no matter
		 *  of intensity has been set by the user.
		 */
		public function getForce(): Number {
			if(getFuel() <= 0)
				return 0;
			return force;
		}

		/**
		 *  Get's engine's friction coefficient for linear movement.
		 *  The idea is following: if linear velocity is to big,
		 *  we have friction factor, slowing engine's efficiency.
		 */
		public function getForceFriction(): Number{
			return forceFriction;
		}

		/**
		 *  Turn engine on for rotation to given intensity.
		 *  @param coefficient the intensity of engine, where -1 is engine is on for rotation counterclockwise,
		 *         0 is rotation off, and 1 is engine is set to full power to clockwise rotation.
		 */
		public function enableTorque(coefficient: Number): void {
			var newTorque = torqueMax * coefficient;
			if(coefficient >= 0)
				newTorque = Math.min(torqueMax, newTorque);
			else
				newTorque = Math.max(-torqueMax, newTorque);

			this.torque = newTorque;
		}

		/**
		 *  Turn engine off for rotation. Equivalent to enableTorque(0).
		 */
		public function disableTorque(): void{
			this.torque = 0;
		}

		/**
		 *  Returns angular force of engine.
		 *  Be aware, that for playability, we choose, that if no fuel,
		 *  engine is still able to rotate the ship.
		 */
		public function getTorque(): Number {
			return torque;
		}

		/**
		 *  Get's engine's friction coefficient for angular movement.
		 *  The idea is following: if rotation velocity is to big,
		 *  we have friction factor, slowing engine's efficiency.
		 */
		public function getTorqueFriction(): Number{
			return torqueFriction;
		}

        public function setFuel(value: Number){
            this.fuel = value;
        }

		/**
		 * Returns fuel left in the engine.
		 */
		public function getFuel(): Number{
			return this.fuel;
		}

        public function getMaxFuel(): Number{
            return this.fuelMax;
        }

		/**
		 *  According to current engine's working intensity, loose fuel.
		 *  @param Time in milisecundes.
		 */
		public function useFuel(deltaT: Number): void{
			var usedFuel: Number = (this.force / this.forceMax)*deltaT;
			this.fuel -= usedFuel;
			if(this.fuel < 0)
				this.fuel = 0;
		}

		/**
		 * Load engine with fuel.
		 * If loading more, that it can hold, just load maximum possible value.
		 */
		public function increaseFuel(deltaFuel: Number):void{
			this.fuel += deltaFuel;
			if(getFuel() > this.fuelMax)
				this.fuel = this.fuelMax;
		}

	/**
	 * This method returns short information about engine as a string.
	 * Only information relevant to user-side included.
	 */
        public function info(): String{
        	var result: String = new String();
        	result += "Engine:\n";
        	result += "Force: " + this.forceMax + "\n";
        	result += "Torque: " + this.torqueMax + "\n";
        	result += "Force friction: " + this.getForceFriction() + "\n";
        	result += "Torque friction: " + this.getTorqueFriction() + "\n";
        	result += "Fuel: " + this.getFuel() + " / " + this.getMaxFuel() + "\n";

        	return result;
        }

        public function setup(ship: Ship): void {
            ship.setEngine(this);
        }

        public function setupable(ship: Ship): Boolean {
            return true;
        }

	}

}
