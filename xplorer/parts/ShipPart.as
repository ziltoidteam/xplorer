﻿package xplorer.parts {
    import xplorer.game.objects.Ship;

	public interface ShipPart {
        function clone(): ShipPart;
        function info(): String;
        function setup(ship: Ship): void;
        function setupable(ship: Ship): Boolean;
	}
}
