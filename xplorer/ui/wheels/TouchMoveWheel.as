﻿package xplorer.ui.wheels {
    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.BitmapDataChannel;
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.TouchEvent;
    import xplorer.system.Vector2d;
    import xplorer.game.objects.FlyingObject;

	public class TouchMoveWheel extends TouchWheel {
		private var controlVector: Vector2d = new Vector2d(0,0);

		public function TouchMoveWheel(scene: DisplayObject, controlled: FlyingObject, center: Vector2d, radius: Number) {
			super(scene, controlled, center, radius);
		}

		override protected function init(): void{
			var bitmap:BitmapData = new BitmapData(getRadius() * 2, getRadius() * 2, true);

            //myBitmap.noise(500, 0, 255, BitmapDataChannel.BLUE,false);
			for(var yy = 0; yy < 2*getRadius(); ++yy)
				for(var xx = 0; xx < 2*getRadius(); ++xx){
					var pos: Vector2d = new Vector2d(getRadius() - xx, getRadius() - yy);
					var norm: Number = pos.norm;

					var radialComponent: Number = formFunction(norm/getRadius())
					bitmap.setPixel32(xx,yy,convertRGBAtoHEX(255*radialComponent,255*radialComponent,255*radialComponent, 50));
				}

            var image:Bitmap = new Bitmap(bitmap);

			image.width = 2 * getRadius();
			image.height = 2 * getRadius()
			image.x = getCenter().x - getRadius();
			image.y = getCenter().y - getRadius();

			getScene().addEventListener(TouchEvent.TOUCH_BEGIN,onTouchDown);
		    getScene().addEventListener(TouchEvent.TOUCH_END,onTouchUp);

            this.addChild(image);
		}

		override protected function formFunction( value: Number): Number{
			var centralLimit: Number = 0.1;
			var sideRing:Number = 0.9;

			if(value <= centralLimit)
				return 1;

			if(value >= centralLimit && value <= sideRing){
				var t1:Number = -Math.pow((value - centralLimit)/((sideRing - centralLimit)/3),2);
				var t2:Number = -Math.pow((-value + sideRing)/((sideRing - centralLimit)/6),2);
				return (Math.exp(t1)+0.3*Math.exp(t2));
			}

			return 0;
		}

		function onTouchDown(e:TouchEvent) {
			// User must press inside the control wheel to begin moving
			if ( distanceFromCenter(e.stageX, e.stageY) >= getRadius() ) return;

			this.setTouchID(e.touchPointID);
			getScene().addEventListener(TouchEvent.TOUCH_MOVE, onTouchMove);
			getScene().addEventListener(Event.ENTER_FRAME, onControllingObject);
			updateControlVector(e);
		}

		function onTouchMove(e:TouchEvent) {
			if(!this.matchTouchID(e.touchPointID)) return ;

			updateControlVector(e);
		}

		function onTouchUp(e:TouchEvent) {
			if(!this.matchTouchID(e.touchPointID)) return ;

			getScene().removeEventListener(Event.ENTER_FRAME, onControllingObject);
			getScene().removeEventListener(TouchEvent.TOUCH_MOVE, onTouchMove);
			getControlledObject().getEngine().disableTorque();
			getControlledObject().getEngine().disableForce();
			controlVector = new Vector2d(0,0);
		}

		function onControllingObject(e:Event) {
			// At PI we have maximum torque power. That's why we devide angle by 2
			var engineTorque: Number = - Math.sin(getControlledObject().getRotationAngle() + Math.PI/2 - controlVector.angle);

			// Taking care of proection to ship's direction. Wheel radius is
			var engineForce: Number = Math.cos(getControlledObject().getRotationAngle() + Math.PI/2 - controlVector.angle) * (controlVector.norm / (getRadius()));
			if( engineForce > 1 )
				engineForce = 1;

			this.dispatchEvent(new MoveWheelEvent(MoveWheelEvent.changing, engineForce, engineTorque));
		}

		private function updateControlVector(e: TouchEvent): void{
			controlVector = new Vector2d(e.stageX - getCenter().x, e.stageY - getCenter().y);
		}

	}


}
