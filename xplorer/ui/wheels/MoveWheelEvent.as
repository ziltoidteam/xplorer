﻿package xplorer.ui.wheels {
	import flash.events.Event;
    import xplorer.game.objects.SpaceObject;

	public class MoveWheelEvent extends Event
	{
		public static const changing:String = "ChangingEvent";

		public var force: Number;
		public var torque: Number;

		public function MoveWheelEvent(type:String, force: Number, torque: Number){
			super(type, true, false);
			this.force = force;
			this.torque = torque;
		}

		override public function clone():Event{
			return new MoveWheelEvent(this.type, this.force, this.torque);
		}

	}
}
