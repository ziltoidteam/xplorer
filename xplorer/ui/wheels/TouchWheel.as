﻿package xplorer.ui.wheels {
    import flash.display.Sprite;
	import flash.display.DisplayObject;
    import xplorer.system.Vector2d;
    import xplorer.game.objects.FlyingObject;

	public class TouchWheel extends Sprite {
		private var scene: DisplayObject = null;
		private var controlled: FlyingObject = null;
		private var center: Vector2d = null;
		private var radius: Number = 0;

		private var touchID: Number = 0;

		public function TouchWheel(scene: DisplayObject, controlled: FlyingObject, center: Vector2d, radius: Number) {
			this.scene = scene;
			this.controlled = controlled;
			this.center = center;
			this.radius = radius;

			init();
		}

		// Virtual
		protected function init(): void{
			// empty
		}

		// Virtual
		protected function formFunction( value: Number): Number{
			return 0;
		}

		protected static function convertRGBAtoHEX(r: Number, g: Number, b: Number, a: Number): uint{
			return ((a << 24) | (r << 16) | (g << 8) | b);
		}

		protected function distanceFromCenter(x: Number, y:Number): Number{
			var radiusVector: Vector2d = new Vector2d( x - this.getCenter().x, y - this.getCenter().y);
			return radiusVector.norm;
		}

		protected function getScene(): DisplayObject{
			return this.scene;
		}

		protected function getControlledObject(): FlyingObject{
			return this.controlled;
		}

		protected function getCenter(): Vector2d{
			return this.center;
		}

		protected function getRadius(): Number{
			return this.radius;
		}

		protected function setTouchID(touchID: Number){
			this.touchID = touchID;
		}

		protected function getTouchID(): Number{
			return this.touchID;
		}

		protected function matchTouchID(touchID: Number): Boolean{
			return (touchID == this.touchID)
		}

	}


}
