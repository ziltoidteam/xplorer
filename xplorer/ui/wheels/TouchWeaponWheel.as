﻿package xplorer.ui.wheels {
    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.BitmapDataChannel;
	import flash.display.DisplayObject;
	import flash.events.TouchEvent;
	import flash.events.Event;
    import xplorer.system.Vector2d;
    import xplorer.game.objects.FlyingObject;

	public class TouchWeaponWheel extends TouchWheel {

		public function TouchWeaponWheel(scene: DisplayObject, controlled: FlyingObject, center: Vector2d, radius: Number) {
			super(scene, controlled, center, radius);
		}

		override protected function init(): void{
			var bitmap:BitmapData = new BitmapData(getRadius() * 2, getRadius() * 2, true);

            //myBitmap.noise(500, 0, 255, BitmapDataChannel.BLUE,false);
			for(var yy = 0; yy < 2*getRadius(); ++yy)
				for(var xx = 0; xx < 2*getRadius(); ++xx){
					var pos: Vector2d = new Vector2d(getRadius() - xx, getRadius() - yy);
					var norm: Number = pos.norm;

					var radialComponent: Number = formFunction(norm/getRadius())
					bitmap.setPixel32(xx,yy,convertRGBAtoHEX(255*radialComponent,255*radialComponent,255*radialComponent, 50));
				}

            var image:Bitmap = new Bitmap(bitmap);

			image.width = 2 * getRadius();
			image.height = 2 * getRadius()
			image.x = getCenter().x - getRadius();
			image.y = getCenter().y - getRadius();

			getScene().addEventListener(TouchEvent.TOUCH_TAP, onTouchTap);

            this.addChild(image);
		}

		override protected function formFunction( value: Number): Number{
			var centralLimit: Number = 0.5;

			if(value <= centralLimit)
				return 1;

			return 0;
		}

		function onTouchTap(e:TouchEvent) {
			// User must press inside the control wheel to begin shooting
			if ( distanceFromCenter(e.stageX, e.stageY) >= getRadius() ) return;

			this.setTouchID(e.touchPointID); // useless now but may be use in future

			this.dispatchEvent(new WeaponWheelEvent(WeaponWheelEvent.fire));
		}


	}


}
