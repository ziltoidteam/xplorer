﻿package xplorer.ui.wheels {
	import flash.events.Event;
    import xplorer.game.objects.SpaceObject;

	public class WeaponWheelEvent extends Event
	{
		public static const fire:String = "FireEvent";

		public function WeaponWheelEvent(type:String){
			super(type, true, false);
		}

		override public function clone():Event{
			return new WeaponWheelEvent(this.type);
		}

	}
}
