﻿package xplorer.ui.windows {
    import xplorer.game.objects.Ship;
    import xplorer.system.Vector2d;
    import xplorer.missions.Mission;

    public class GameWindow extends Window {
        var mission: Mission;

        public function GameWindow(mission: Mission) {
            this.mission = mission;
            this.add_content(mission);

            this.addUI();
        }

        private function addUI(): void {
            var backButton: Button = new Button(new BackButton(), new Vector2d(6.55, 5.55), this);
            backButton.onClickEvent = deactivate;

            var restartButton: Button = new Button(new RestartButton(), new Vector2d(6.55, 68.45), this);
            restartButton.onClickEvent = this.mission.restart;
        }
    }
}
