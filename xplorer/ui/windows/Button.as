﻿package xplorer.ui.windows {
    import flash.display.MovieClip;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import xplorer.ui.windows.Window;
    import xplorer.system.Vector2d;

    public class Button extends MovieClip {
        private var onClick: Function = null;

        public function Button( graphic: MovieClip = null, position: Vector2d = null, window: Window = null ) {
            this.addEventListener( Event.ADDED_TO_STAGE, onInitListener );
            this.addEventListener( Event.REMOVED_FROM_STAGE, onRemoveListener );

            this.setGraphic(graphic);
            this.setPosition(position);
            this.setWindow(window, position);
        }

        public function set onClickEvent( f: Function ) {
            this.onClick = f;
        }

        // Private part
        private function setGraphic( graphic: MovieClip ): void {
            this.clear();

            if (graphic != null) this.addChild(graphic);
        }

        private function clear() {
            while(this.numChildren > 0){
                this.removeChildAt(0);
            }
        }

        private function onInitListener( e: Event ) {
            this.addEventListener( MouseEvent.CLICK, onMouseClick );
            this.addEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
        }

        private function onRemoveListener( e: Event ) {
            this.removeEventListener( MouseEvent.CLICK, onMouseClick );
            this.removeEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
        }

        private function onMouseClick( e: Event ) {
            if (this.onClick != null) this.onClick.call();
        }

        private function onMouseDown( e: Event ) {
            this.x += 2; // TODO: Remove magic number!
            this.y += 2;
            stage.addEventListener( MouseEvent.MOUSE_UP, onMouseUp );
        }

        private function onMouseUp( e: Event ) {
            this.x -= 2;
            this.y -= 2;
            stage.removeEventListener( MouseEvent.MOUSE_UP, onMouseUp );
        }

        private function setPosition(position: Vector2d) {
            if (position != null) {
                this.x = position.x;
                this.y = position.y;
            } else {
                this.x = this.y = 0; // WAT? Maybe best is no change position?
            }
        }

        private function setWindow(window: Window, position: Vector2d = null) {
            var newPos: Vector2d;
            if (position != null) newPos = position;
            else newPos = new Vector2d(0, 0);
            if (window != null) {
                window.add_content(this, newPos.x, newPos.y);
            }
        }
    }
}
