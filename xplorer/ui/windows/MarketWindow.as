﻿package xplorer.ui.windows {
	import xplorer.ui.grid.ItemGridEvent;
	import flash.events.Event;
	import xplorer.system.Vector2d;
	import xplorer.ui.grid.ItemGrid;
	import xplorer.parts.ShipPart;
	import xplorer.ui.Tip;
    import xplorer.game.objects.Ship;
    import xplorer.game.Game;
    import xplorer.ui.TextBox;
    import xplorer.ui.indicators.TextIndicator;

	//remove
	import xplorer.parts.engines.Engine;
	import xplorer.parts.weapons.*;
	import xplorer.game.Director;
	import xplorer.system.Random;

	public class MarketWindow extends Window{
		var grid: ItemGrid;
        var selectedItem: ShipPart = null;
		var player: Ship;
        var setupButton: Button;
        var textBox: TextBox;
        var scoreIndicator: TextIndicator = new TextIndicator("Score: ");

		public function MarketWindow(player: Ship) {
			this.player = player;
            this.addEventListener( ItemGridEvent.ON_ITEM_SELECT, onInventorySelect);

            this.addUI();

            this.spawnRandom();

            scoreIndicator.setValue(String(player.getLut().getScore()));
		}

        private function addUI(): void {
            var bg = Game.create_bg(800,480);
            this.add_content(bg, 400, 240);

            this.textBox = new TextBox(200);
            this.add_content(this.textBox, 590, 10);

            this.add_content(this.scoreIndicator, 100, 100);

            var backButton: Button = new Button(new BackButton(), new Vector2d(6.55, 5.55), this);
            backButton.onClickEvent = deactivate;

            setupButton = new Button(new PlayButton(), new Vector2d(650, 300), this);
            setupButton.onClickEvent = setupHandler;
            this.setupButton.visible = false;

            const invWidth: int = 4;
            const invHeigth: int = 6;
            grid = new ItemGrid(invWidth,invHeigth);
            this.add_content(grid);
            grid.x = this.width / 2 - grid.width / 2;
            grid.y = this.height / 2 - grid.height / 2;
        }

        private function onInventorySelect(e: ItemGridEvent): void {
            var info: String = e.item.info();

            if(e.item is WeaponObject) {
                info += "\n\n" + player.getActiveWeapon().info();
            } else if (e.item is Engine) {
                info += "\n\n" + player.getEngine().info();
            }

            this.textBox.setText(info);

            this.selectedItem = e.item;
            this.setupButton.visible = true;
        }

        private function setupHandler(): void {
            const cost: Number = 100;
            if (player.getLut().getScore() < cost) {
                var tip: Tip = new Tip("Information", "You don't have enouch money!", 13, 100, true);
                this.add_content(tip);

                return;
            }

            player.getLut().addScore(-cost);
            scoreIndicator.setValue(String(player.getLut().getScore()));

            this.selectedItem.setup(player);
            this.grid.remove(this.selectedItem);

            var tip: Tip = new Tip("Information", "Selected item sucessfully setuped!", 13, 100, true);
            this.add_content(tip);

            this.setupButton.visible = false;
        }

		private function spawnRandom():void {
			for (var x = 0; x < grid.getSize().x; ++x) {
				for (var y = 0; y < grid.getSize().y; ++y) {
					var part: ShipPart;
					const magic: Number = Random.randomRange(0,100);
					if( magic < 25){
						part = Director.getShipEngine(100);
					}
					else if( magic < 50 ) {
						part = Director.getRocketLauncher(100);
					}
					else if( magic < 75 ) {
						part = Director.getMachineGun(100);
					}
					else {
						part = Director.getMineShooter(100);
					}
		            grid.set(part,x,y);
				}
			}
		}
	}
}
