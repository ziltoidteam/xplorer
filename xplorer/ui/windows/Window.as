﻿package xplorer.ui.windows {
	import flash.display.MovieClip;

    public class Window {
        private var content_container: MovieClip = new MovieClip();

        public function Window( create_active: Boolean = false ) {
            if (create_active) this.activate();
        }

        public function activate(): void {
            WindowSystem.activate_window(this);
        }

        public function deactivate(): void {
            if (this.is_active()) {
                WindowSystem.deactivate_active_window();
            }
        }

        public function add_content( content: MovieClip, x: Number = 0, y: Number = 0 ): void {
            this.content_container.addChild(content);
            if (x != 0) content.x = x;
            if (y != 0) content.y = y;
        }

        public function remove_content( content: MovieClip ): void {
            this.content_container.removeChild(content);
        }

        public function get_content_container(): MovieClip {
            return this.content_container;
        }

        public function clear_window(): void {
            while(this.content_container.numChildren > 0){
                this.content_container.removeChildAt(0);
            }
        }

        public function is_active(): Boolean {
			return WindowSystem.get_active_window() == this;
        }

        public function addEventListener(event, func): void {
            this.content_container.addEventListener(event, func);
        }

        public function removeEventListener(event, func): void {
            this.content_container.removeEventListener(event, func);
        }

        public function get width(): Number {
            return WindowSystem.stage.stageWidth;
        }

        public function get height(): Number {
            return WindowSystem.stage.stageHeight;
        }
    }
}
