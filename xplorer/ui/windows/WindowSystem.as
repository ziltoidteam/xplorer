﻿package xplorer.ui.windows {
    import flash.display.Stage;

    public class WindowSystem {
        public static var stage: Stage = null;
        private static var windows_cycle: Vector.<Window> = new Vector.<Window>();

        public static function start( stage: Stage ): void {
			WindowSystem.stage = stage;
        }

        public static function activate_window( window: Window ): void {
            if (WindowSystem.get_active_window() == window
		|| window == null) return;

            if (!WindowSystem.is_empty())
                WindowSystem.hide_window_content(WindowSystem.get_active_window());

            if (WindowSystem.has(window)) {
                WindowSystem.move_on_top(window);
            } else {
                WindowSystem.add_on_top(window);
            }

            WindowSystem.show_window_content(window);
        }

        public static function deactivate_active_window(): void {
            if (WindowSystem.is_empty()) return;

            WindowSystem.hide_window_content(WindowSystem.get_active_window());
            WindowSystem.move_down(WindowSystem.get_active_window());
            WindowSystem.show_window_content(WindowSystem.get_active_window());
        }

		public static function get_active_window(): Window {
			if (WindowSystem.is_empty())
				return null;
			else
				return WindowSystem.windows_cycle[WindowSystem.windows_cycle.length - 1];
        }

        public function get_width(): Number {
            return WindowSystem.stage.stageWidth;
        }

        public function get_height(): Number {
            return WindowSystem.stage.stageHeight;
        }

        private static function move_on_top(window: Window): void {
            const window_index: Number = WindowSystem.windows_cycle.indexOf(window);
            WindowSystem.windows_cycle.splice(window_index, 1);
            WindowSystem.add_on_top(window);
        }

        private static function move_before(window: Window): void {
            const window_index: Number = WindowSystem.windows_cycle.indexOf(window);
            WindowSystem.windows_cycle.splice(window_index, 1);
            const new_index: Number = Math.max(window_index - 1, 0);
            WindowSystem.windows_cycle.splice(new_index, 0, window);
        }

        private static function move_down(window: Window): void {
            const window_index: Number = WindowSystem.windows_cycle.indexOf(window);
            WindowSystem.windows_cycle.splice(window_index, 1);
            WindowSystem.windows_cycle.splice(0, 0, window);
        }

        private static function add_on_top(window: Window): void {
            WindowSystem.windows_cycle.push(window);
        }

        private static function is_empty(): Boolean {
            return WindowSystem.windows_cycle.length == 0;
        }

		private static function has( window: Window ): Boolean {
			return WindowSystem.windows_cycle.indexOf(window) >= 0;
		}

        private static function show_window_content( window: Window ): void {
            stage.addChild(window.get_content_container());
        }

        private static function hide_window_content( window: Window ): void {
            stage.removeChild(window.get_content_container());
        }
    }
}
