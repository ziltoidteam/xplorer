﻿package xplorer.ui.windows {
	import xplorer.system.Vector2d;
	import xplorer.missions.*;
	import xplorer.game.objects.Ship;

	import xplorer.system.Random;

	public class MissionsWindow extends Window {
		var player: Ship;
        var goButton: Button;

		public function MissionsWindow(player: Ship) {
			this.player = player;

            this.addUI();
		}

        private function addUI(): void {
            var backButton: Button = new Button(new BackButton(), new Vector2d(6.55, 5.55), this);
            backButton.onClickEvent = deactivate;

            goButton = new Button(new PlayButton(), new Vector2d(400, 200), this);
            goButton.onClickEvent = goHandler;
        }

        private function goHandler(): void {
            var mission: Mission = new SurviveInTime(player, 100, 120);
            var game: GameWindow = new GameWindow(mission);
            game.activate();
        }
	}
}
