﻿package xplorer.ui.windows {
    import xplorer.system.Vector2d;
    import xplorer.game.Game;
    import xplorer.game.objects.Ship;
    import xplorer.game.Director;

    public class MainWindow extends Window {
        var player: Ship = Director.generateShip(0);

        public function MainWindow() {
            this.addUI();
            // @TODO: Remove this
            player.getLut().addScore(1000);
        }

        private function addUI() {
            var bg = Game.create_bg(800,480);
            this.add_content(bg, 400, 240);

            var playButton: Button = new Button(new PlayButton(), new Vector2d(347.95, 179), this);
            playButton.onClickEvent = playHandler;

            var marketButton: Button = new Button(new BackButton(), new Vector2d(147.95, 79), this);
            marketButton.onClickEvent = marketHandler;
        }

        private function playHandler(): void {
            var selector: Window = new MissionsWindow(this.player);

            selector.activate();
        }

        private function marketHandler(): void {
            var market: Window = new MarketWindow(this.player);

            market.activate();
        }
    }
}
