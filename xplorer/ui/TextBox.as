﻿package xplorer.ui {
    import flash.display.MovieClip;
    import flash.text.TextField;
    import flash.text.TextFormat;
    import flash.text.TextFieldAutoSize;

	public class TextBox extends MovieClip {
        private var textField = new TextField();


		public function TextBox(maxWidth: Number) {
            var format:TextFormat = new TextFormat();
            format.color = 0xFFFFFF;
            format.size = 13;
            format.font = (new Veranda()).fontName;

            textField.defaultTextFormat = format;
            textField.wordWrap = true;
            textField.border = true;
            textField.borderColor = 0xFFFFFF;
            textField.selectable = false;
            textField.autoSize = TextFieldAutoSize.LEFT;
            textField.width = maxWidth;
            this.addChild(textField);
		}

        public function setText(text: String): void {
            textField.text = text;
        }
	}

}
