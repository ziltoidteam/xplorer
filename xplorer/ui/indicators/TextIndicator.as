﻿package xplorer.ui.indicators {
	import flash.text.TextField;
	import flash.text.TextFormat;
    import flash.text.TextFieldAutoSize;
	import flash.text.TextFormatAlign;

	public class TextIndicator extends IndicatorBase {
        var valueTextField: TextField = new TextField();

		public function TextIndicator(name: String, value: String = "", nameFontSize: Number = IndicatorBase.baseNameSize, valueFontSize: Number = IndicatorBase.baseValueSize) {
            super(name, nameFontSize);

            this.addChild(this.valueTextField);

            this.setValueSize(valueFontSize);
            this.setValue(value);

            this.updateValuePosition();
		}

        protected function setValueSize(valueFontSize: Number) {
            var format:TextFormat = new TextFormat();
            format.color = 0xFFFFFF;
            format.size = valueFontSize;
            format.font = (new Veranda()).fontName;
            format.align = TextFormatAlign.LEFT;

            this.valueTextField.defaultTextFormat = format;
            this.valueTextField.autoSize = TextFieldAutoSize.LEFT;
        }

        protected function updateValuePosition() {
            this.valueTextField.x = this.getOpenPartX();
            this.valueTextField.y = (this.nameTextField.height - this.valueTextField.height) / 2;
        }

        override public function setName(name: String) {
            super.setName(name);

            updateValuePosition();
        }

        public function setValue(value: String) {
            this.valueTextField.text = value;

            updateValuePosition();
        }
	}
}
