﻿package xplorer.ui.indicators {
	public class RangeIndicator extends IndicatorBase {
		var line: IndicatorLine = new IndicatorLine();
		var scroll: IndicatorScroll = new IndicatorScroll();
		var lbracket: IndicatorBracket = new IndicatorBracket();
		var rbracket: IndicatorBracket = new IndicatorBracket();

		public function RangeIndicator(name: String, length: Number, nameFontSize: Number = IndicatorBase.baseNameSize) {
            super(name, nameFontSize);

			this.line.width = length;
			this.addChild(line);
			this.addChild(scroll);
			this.addChild(lbracket);
			this.addChild(rbracket);

			this.setName(name);

			this.setScroll(0);
		}

        protected function updateScrollPartPosition() {
            this.line.x = this.getOpenPartX();
            this.lbracket.x = this.line.x;
            this.rbracket.x = this.line.x + this.line.width;

            this.line.y     = this.nameTextField.height / 2;
            this.lbracket.y = this.nameTextField.height / 2;
            this.rbracket.y = this.nameTextField.height / 2;
            this.scroll.y   = this.nameTextField.height / 2;
        }

        override public function setName(name: String) {
            super.setName(name);

            updateScrollPartPosition();
        }

        public function setScroll(value: Number) { // In percent
            updateScrollPartPosition();

            if (value < 0 || value > 100)
            	throw "RangeIndicator(): i can use percents, more than 100 or less than 0, stupid.";

            const min: Number = this.line.x;
            const max: Number = min + this.line.width;
            const onePercent: Number = (max - min) / 100;
            const position: Number = min + onePercent * value;

            this.scroll.x = position;
        }
	}
}
