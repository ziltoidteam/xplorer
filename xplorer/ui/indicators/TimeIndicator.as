﻿package xplorer.ui.indicators {
	public class TimeIndicator extends TextIndicator {
		public function TimeIndicator(name: String, nameFontSize: Number = IndicatorBase.baseNameSize, valueFontSize: Number = IndicatorBase.baseValueSize) {
			super(name, "", nameFontSize, valueFontSize);
		}

        public function setTime(time: Number) {
            var minutes: int = time / 60;
            var seconds: int = time % 60;
            this.setValue(minutes + ":" + seconds);
        }
	}
}
