﻿package xplorer.ui.indicators {
	public class CheckBoxIndicator extends TextIndicator {
        var checkBox: CheckBox = new CheckBox();

		public function CheckBoxIndicator(text: String, textFontSize: Number = IndicatorBase.baseValueSize, checkBoxSize: Number = 0.2) {
            this.checkBox.scaleX = this.checkBox.scaleY = checkBoxSize;

            this.addChild(this.checkBox);
            this.checkBox.y = 40 * this.checkBox.scaleY;
            this.checkBox.x = -(this.checkBox.width * this.checkBox.scaleX) - 5;

            this.setChecked(false);

            super("", text, 12, textFontSize);
		}

        override protected function updateValuePosition() {
            this.valueTextField.x = 40 * this.checkBox.scaleX + 5 + this.checkBox.x;
            this.valueTextField.y = this.checkBox.y - this.valueTextField.height / 2;
        }

        public function setChecked(checked: Boolean) {
            this.checkBox.mark.visible = checked;
        }
	}
}
