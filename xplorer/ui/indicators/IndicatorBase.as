﻿package xplorer.ui.indicators {
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFormat;
    import flash.text.TextFormatAlign;
    import flash.text.TextFieldAutoSize;

	public class IndicatorBase extends MovieClip {
        var nameTextField: TextField = new TextField();
        public static const baseNameSize: Number = 13;
        public static const baseValueSize: Number = 13;

		public function IndicatorBase(name: String, nameFontSize: Number = baseNameSize) {
            this.addChild(this.nameTextField);

            this.setNameSize(nameFontSize);
            this.setName(name);
		}

        protected function setNameSize(nameFontSize: Number) {
            var format:TextFormat = new TextFormat();
            format.color = 0xFFFFFF;
            format.size = nameFontSize;
            format.font = (new Veranda()).fontName;
            format.align = TextFormatAlign.RIGHT;

            this.nameTextField.defaultTextFormat = format;
            this.nameTextField.autoSize = TextFieldAutoSize.RIGHT;
        }

        protected function getOpenPartX() {
            return this.nameTextField.x + this.nameTextField.width + 5;
        }

        public function setName(name: String) {
            this.nameTextField.text = name;

            this.nameTextField.x = -this.nameTextField.width;
        }
	}
}
