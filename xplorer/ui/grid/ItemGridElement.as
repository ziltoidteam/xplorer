﻿package xplorer.ui.grid {
    import flash.display.MovieClip;
    import flash.events.MouseEvent;

    import xplorer.system.Vector2d;
    import xplorer.parts.ShipPart;

    public class ItemGridElement extends MovieClip {
        private var item: ShipPart;
        private var size: Vector2d;

        public function ItemGridElement(size: Vector2d): void {
            this.size = size;
            this.unset();
        }

        public function set(item: ShipPart): void {
            if (item == null)
                throw "ItemGridElement: setting item to null";

            // Removing old event listener
            if (this.item != null)
                this.removeEventListener(MouseEvent.CLICK, onButtonClick);

            this.item = item.clone();
            setFilledGraphics(this.item);
            this.addEventListener(MouseEvent.CLICK, onButtonClick);
        }

        public function unset(): void {
            if (this.item != null) {
                this.removeEventListener(MouseEvent.CLICK, onButtonClick);
            }

            this.item = null;
            setEmptyGraphics();
        }

        public function get(): ShipPart {
            return this.item;
        }

        protected function setEmptyGraphics(): void {
            this.graphics.clear();
            this.graphics.lineStyle(2, 0xCCCCCC);
            this.graphics.beginFill(0xA0A0A0);
            this.graphics.drawRect(0, 0, size.x, size.y);
            this.graphics.endFill();
        }

        protected function setFilledGraphics(item: ShipPart): void {
            this.graphics.clear();
            this.graphics.lineStyle(2, 0xCCCCCC);
            this.graphics.beginFill(0xA0FFA0);
            this.graphics.drawRect(0, 0, size.x, size.y);
            this.graphics.endFill();
        }

        private function onButtonClick(e: MouseEvent): void {
            dispatchEvent(new ItemGridEvent(ItemGridEvent.ON_ITEM_SELECT, get()));
        }
    }
}
