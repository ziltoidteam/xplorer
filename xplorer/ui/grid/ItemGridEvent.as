﻿package xplorer.ui.grid {
	import flash.events.Event;

    import xplorer.parts.ShipPart;

	public class ItemGridEvent extends Event {
		public static const ON_ITEM_SELECT: String = "ON_ITEM_SELECT";
        public var item: ShipPart;

		public function ItemGridEvent(type: String, item: ShipPart) {
			super(type, true, false);

            this.item = item;
		}

		override public function clone():Event{
			return new ItemGridEvent(this.type, this.item);
		}
	}
}
