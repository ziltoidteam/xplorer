﻿package xplorer.ui.grid {
    import flash.display.MovieClip;
    import xplorer.ui.grid.ItemGridElement;

    import xplorer.system.Vector2d;
    import xplorer.parts.ShipPart;

    public class ItemGrid extends MovieClip {
        private var items: Vector.<Vector.<ItemGridElement>> = null;

        private var size: Vector2d;
        private const itemSize: Vector2d = new Vector2d(70, 70);
        private const interval: Vector2d = new Vector2d(5, 5);

        public function ItemGrid(widthInCells: int, heightInCells: int) {
            if( widthInCells < 0 || heightInCells < 0 || widthInCells == 0 && heightInCells == 0)
                throw "ItemGrid: invalid size";

            this.size = new Vector2d(widthInCells, heightInCells);

            this.initialize();
        }

        public function getSize(): Vector2d {
            return this.size;
        }

        public function unset(xCell: int, yCell: int): void {
            if( xCell < 0 || xCell >= size.x ||
                yCell < 0 || yCell >= size.y)
                throw "ItemGrid: Error, out of grid bounds";

            if(items[yCell][xCell].get() == null)
                throw "ItemGrid: Error, removing null item";

            items[yCell][xCell].unset();
        }

        public function remove(sp: ShipPart): void {
            for each (var line: Vector.<ItemGridElement> in this.items) {
                for each (var item: ItemGridElement in line) {
                    if (sp == item.get()) {
                        item.unset();
                        return;
                    }
                }
            }
        }

        public function set(item: ShipPart, xCell: int, yCell: int): void {
            if(item == null)
                throw "ItemGrid: Error, adding null item";

            if( xCell < 0 || xCell >= size.x ||
                yCell < 0 || yCell >= size.y)
                throw "ItemGrid: Error, out of grid bounds";

            items[yCell][xCell].set(item);
        }

        public function get(xCell: int, yCell: int): ShipPart {
            if( xCell < 0 || xCell >= size.x ||
                yCell < 0 || yCell >= size.y)
                    throw "ItemGrid: Error, out of grid bounds";

            return items[yCell][xCell].get();
        }

        private function initialize() {
            this.items = new Vector.<Vector.<ItemGridElement>>(size.y,true);
            for( var i: int = 0; i < size.y; ++i){
                items[i] = new Vector.<ItemGridElement>(size.x,true);
                for(var j = 0; j < size.x; ++j){
                    items[i][j] = new ItemGridElement(itemSize);
                    items[i][j].x = j*(itemSize.x + interval.x);
                    items[i][j].y = i*(itemSize.y + interval.y);
                    this.addChild(items[i][j]);
                }
            }
        }
    }
}

