﻿
package xplorer.ui {
    import flash.display.MovieClip;
    import flash.events.Event;
    import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
    import flash.text.TextFormatAlign;
    import flash.text.TextFieldAutoSize;
    import xplorer.traces.ParticleBasic;

	public class Tip extends MovieClip {
        var titleTextField: TextField = new TextField();
        var textField: TextField = new TextField();
        var graphic: MovieClip = new MovieClip();
        var fadeSpeed: Number;

		public function Tip(title: String, text: String, fontSize: Number, fadeSpeed: Number, startWithFading: Boolean = false) {
            this.fadeSpeed = fadeSpeed;

            this.addChild(graphic);
            this.addChild(this.textField);
            this.textField.selectable = false;
            this.addChild(this.titleTextField);
            this.titleTextField.selectable = false;

            this.setFontSize(fontSize);
            this.setText(text);

            this.setTitleFontSize(fontSize);
            this.setTitle(title);

            if (startWithFading) {
                this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
            } else {
                this.addEventListener( MouseEvent.CLICK, onMouseClick );
            }
            this.addEventListener( Event.REMOVED_FROM_STAGE, onRemovedListener );
		}

        protected function onMouseClick(e: Event) {
            this.stage.focus = null;
            this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
        }

        protected function onRemovedListener(e: Event) {
            this.removeEventListener( Event.ENTER_FRAME, onEnterFrame );
        }

        protected function onEnterFrame(e: Event) {
            this.alpha -= this.fadeSpeed / 100 / stage.frameRate;

            if (this.alpha <= 0) {
                this.removeEventListener( Event.ENTER_FRAME, onEnterFrame );
                this.parent.removeChild(this);
            }
        }

        protected function setFontSize(fontSize: Number): void {
            var format:TextFormat = new TextFormat();
            format.color = 0xFFFFFF;
            format.size = fontSize;
            format.font = (new Veranda()).fontName;
            format.align = TextFormatAlign.CENTER;

            this.textField.defaultTextFormat = format;
            this.textField.autoSize = TextFieldAutoSize.LEFT;
        }

        protected function setTitleFontSize(fontSize: Number): void {
            var format:TextFormat = new TextFormat();
            format.color = 0xFFFFFF;
            format.size = fontSize;
            format.font = (new Veranda()).fontName;
            format.align = TextFormatAlign.CENTER;

            this.titleTextField.defaultTextFormat = format;
            this.titleTextField.autoSize = TextFieldAutoSize.LEFT;
        }

        public function setText(text: String): void {
            this.textField.htmlText = text;

            this.updateGraphics();
        }

        public function setTitle(title: String): void {
            this.titleTextField.htmlText = title;
            this.titleTextField.x = (this.textField.width - this.titleTextField.width) / 2;
            this.titleTextField.y = -this.titleTextField.height;

            this.updateGraphics();
        }

        protected function updateGraphics(): void {
            const marign: Number = 5;
            const left: Number = this.textField.x;
            const right: Number = this.textField.width;
            const top: Number = this.titleTextField.y;
            const bottom: Number = this.textField.height - top;

            this.graphic.graphics.clear();
            this.graphic.graphics.lineStyle(2, 0xCCCCCC);
            this.graphic.graphics.beginFill(0x00ABBA);

            this.graphic.graphics.drawRect(
                left - marign,
                top - marign,
                right + marign * 2,
                bottom + marign * 2
            );

            this.graphic.graphics.endFill();

            this.x = (800 - this.width) / 2;
            this.y = (480 - this.height) / 2;
        }
	}

}
