﻿package xplorer.traces {
	import flash.display.MovieClip;
    import flash.events.Event;

    public class TraceBasic extends MovieClip {
        protected var scene: MovieClip;
        protected var fadeSpeed: Number;

        public function TraceBasic(fadeSpeed: Number) {
            this.fadeSpeed = fadeSpeed;

            this.addEventListener(Event.ADDED_TO_STAGE, onAdded);
            this.addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
        }

        private function onAdded(e: Event) {
            this.scene = this.parent.parent as MovieClip;
            this.addEventListener(Event.ENTER_FRAME, onEnterFrame);

			begin();
        }

        private function onRemoved(e: Event) {
            this.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
        }

		protected function begin(): void{
            // virtual
		}

		protected function middle(): void{
            // virtual
		}

		public function end(): void{
            // virtual
		}

        protected function onEnterFrame(e: Event) {
			middle();
        }
    }
}

