package xplorer.traces {
    import flash.display.MovieClip;
    import flash.events.Event;

    public class ParticleBasic extends MovieClip {
        private var fadeSpeed: Number; // percents per second

        public function ParticleBasic(fadeSpeed: Number) {
            this.fadeSpeed = fadeSpeed;
            this.addEventListener( Event.ADDED_TO_STAGE, onAddedListener );
            this.addEventListener( Event.REMOVED_FROM_STAGE, onRemovedListener );
        }

        protected function onEnterFrame(e: Event) {
            this.alpha -= this.fadeSpeed / 100 / stage.frameRate;

            if (this.alpha <= 0) {
                this.removeEventListener( Event.ENTER_FRAME, onEnterFrame );
                this.parent.removeChild(this);
            }
        }

        protected function onAddedListener(e: Event) {
            this.addEventListener( Event.ENTER_FRAME, onEnterFrame );
        }

        protected function onRemovedListener(e: Event) {
            this.removeEventListener( Event.ENTER_FRAME, onEnterFrame );
        }
    }
}

