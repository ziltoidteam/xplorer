﻿package xplorer.traces {
	import flash.display.MovieClip;
    import flash.events.Event;
    import xplorer.system.Vector2d;
    import xplorer.system.Random;

    public class CustomTrace extends TraceBasic {
        private var lastPoint: Vector2d;
        private var Start: Class = null;
        private var Middle: Class = null;
        private var End: Class = null;
        private var interval: Number;

        public function CustomTrace(Start: Class, Middle: Class, End: Class, fadeSpeed: Number, interval: Number) {
            super(fadeSpeed);

            this.interval = interval;

            this.Start = Start;
            this.Middle = Middle;
            this.End = End;
        }

		override protected function begin(): void {
			lastPoint = new Vector2d( this.parent.x, this.parent.y );

            if (this.Start == null) return;
			var startParticle = new Start(fadeSpeed);
            startParticle.x = this.parent.x;
            startParticle.y = this.parent.y;
			startParticle.rotation = this.parent.rotation;
			scene.addChild(startParticle);
			scene.setChildIndex(startParticle, 0);
		}

		override protected function middle(): void {
			// checking scene and parent for null not seems to be needed here

			const currentPoint: Vector2d = new Vector2d( this.parent.x, this.parent.y );
			if (currentPoint.getDistanceTo(lastPoint) <= this.interval)
				return;

            if (this.Middle != null) {
                var particle = new Middle(fadeSpeed);
                particle.scaleX = particle.scaleY = 0.3;
                particle.x = this.parent.x;
                particle.y = this.parent.y;
                particle.rotation = this.parent.rotation;
                scene.addChild(particle);
                scene.setChildIndex(particle, 0);
            }

			lastPoint = currentPoint;
		}

		override public function end(): void{
            if (this.End == null) return;

			var traceEnd = new End(fadeSpeed);
            traceEnd.rotation = this.parent.rotation;
            traceEnd.x = lastPoint.x;
            traceEnd.y = lastPoint.y;
            scene.addChild(traceEnd);
            scene.setChildIndex(traceEnd, 0);
		}
    }
}

