﻿package xplorer.traces {
    import flash.display.MovieClip;
    import flash.events.Event;
    import xplorer.system.Vector2d;

    public class TraceParticle extends ParticleBasic {
        static public var counter: Number = 0;

        public function TraceParticle(startPoint: Vector2d, endPoint: Vector2d, fadeSpeed: Number) {
            super(fadeSpeed);

            this.graphics.clear();
            this.graphics.lineStyle(2, 0xFFFFFF);

            this.graphics.moveTo(startPoint.x, startPoint.y);
            this.graphics.lineTo(endPoint.x, endPoint.y);

            this.graphics.endFill();
        }

        override protected function onAddedListener(e: Event) {
            super.onAddedListener(e);
            counter++;
        }

        override protected function onRemovedListener(e: Event) {
            super.onRemovedListener(e);
            counter--;
        }
    }
}

