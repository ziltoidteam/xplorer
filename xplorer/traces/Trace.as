﻿package xplorer.traces {
    import xplorer.system.Random;
    import xplorer.system.Vector2d;

    public class Trace extends TraceBasic {
        private var lastPoint: Vector2d;

        public function Trace(fadeSpeed: Number) {
            super(fadeSpeed);
        }

		override protected function begin(): void{
			lastPoint = new Vector2d( this.parent.x, this.parent.y );

			var startParticle = new TraceStart(fadeSpeed);
            startParticle.x = this.parent.x;
            startParticle.y = this.parent.y;
			startParticle.rotation = this.parent.rotation;
			scene.addChild(startParticle);
			scene.setChildIndex(startParticle, 0);
		}

		override protected function middle(): void{
			// checking scene and parent for null not seems to be needed here

			const currentPoint: Vector2d = new Vector2d( this.parent.x, this.parent.y );
			if (currentPoint.getDistanceTo(lastPoint) <= 10)
				return;

			var particle = new TraceParticle(currentPoint, lastPoint, fadeSpeed);
			scene.addChild(particle);
            scene.setChildIndex(particle, 0);

			lastPoint = currentPoint;
		}

		override public function end(): void{
			var traceEnd = new TraceEnd(fadeSpeed);
            traceEnd.rotation = this.parent.rotation;
            traceEnd.x = lastPoint.x;
            traceEnd.y = lastPoint.y;
            scene.addChild(traceEnd);
            scene.setChildIndex(traceEnd, 0);
		}
    }
}

