﻿package xplorer.animation {
    import flash.display.DisplayObjectContainer;
    import flash.display.MovieClip;
    import xplorer.system.Vector2d;
	import xplorer.system.Random;

	/**
	 * Class to handle boom animation.
	 */
	public class Boom extends MovieClip {

	/**
	 * Constructor of the class.
	 * @param parent Object we want to animate on it.
	 * @param particlesCount Number of particles in the boom.
	 * @param size Animation radius of the boom.
	 */
        override public function Boom(parent: DisplayObjectContainer, position: Vector2d, particlesCount: Number, size: Number): void {
			if(parent == null)
				return;

            for (var i = 0; i < particlesCount; i++) {
                var d = new BoomPart();
                parent.addChild(d);
                d.x = position.x + Random.randomRange(-20,20);
                d.y = position.y + Random.randomRange(-20,20);
                d.rotation = Random.randomRange(0,360);
                d.scaleX = d.scaleY = Random.randomRange(0.5 * size, 2 * size);
            }
        }
    }
}
