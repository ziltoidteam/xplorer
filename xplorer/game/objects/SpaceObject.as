﻿package xplorer.game.objects {
	import flash.display.MovieClip;
	import flash.events.Event;
	import xplorer.particles.DeathParticle;
    import xplorer.particles.DamageParticle;
    import xplorer.animation.Boom;
    import xplorer.system.Angle;
    import xplorer.system.Vector2d;
    import xplorer.game.requests.GameRequest;
    import xplorer.game.Lut;
    import xplorer.game.ShockWave;

	/**
	 * Base class for all physical objects in the game.
	 * Supports moving, rotating and more.
	 */
	public class SpaceObject extends MovieClip {
		private var healthPoints: Number;
		private var maxHealthPoints: Number;
		private const MIN_HEALTH_POINTS: Number = 0;

		private var damageValue: Number;
		private var interactionRadius;

		private var mass: Number;
		private var momentOfInertia: Number

		private var position: Vector2d;
		private var rotationAngle: Number;

		private var velocity: Vector2d;
		private var anglularVelocity: Number;

		// This is the external forces and torques
		private var externalForce: Vector2d;
		private var externalTorque: Number;

		private var lut: Lut = new Lut();

		// Constructor
		public function SpaceObject(objectMass: Number, objectMomentOfInertia: Number,
									position: Vector2d, rotationAngle: Number, velocity: Vector2d, angularVelocity: Number,
									externalForce: Vector2d, externalTorque: Number,
									interactionRadius: Number, damageValue: Number,
									healthPoints: Number,  maxHealthPoints: Number, lut: Lut ) {
			setMass(objectMass);
			setMomentOfInertia(objectMomentOfInertia);

			setPosition( position );
			setRotationAngle( rotationAngle );

			setVelocity( velocity );
			setAngularVelocity( angularVelocity );

			setExternalForce( externalForce );
			setExternalTorque( externalTorque );

			setInteractionRadius(interactionRadius);
			setDamageValue(damageValue);

			// At first we must to assign max health poins value.
			setMaxHealthPoints(maxHealthPoints);
			setHealthPoints(healthPoints);

			setLut( lut );

			this.addEventListener( Event.ADDED_TO_STAGE, onInitListener );
			this.addEventListener( Event.REMOVED_FROM_STAGE, onRemovedListener );
		}

		public function clone(): SpaceObject{
			return new SpaceObject(getMass(), getMomentOfInertia(), getPosition(), getRotationAngle(),
								   getVelocity(), getAngularVelocity(), getExternalForce(), getExternalTorque(),
								   getInteractionRadius(), getDamageValue(), getHealthPoints(), getMaxHealthPoints(), lut);
		}

		// TODO: Replace iteraction radius model with other model?
		/**
		 * Set interaction radius for an object.
		 * @param interactionRadius New interaction radius.
		 */
		public function setInteractionRadius( interactionRadius: Number ): void {
			this.interactionRadius = interactionRadius;
		}

		public function getInteractionRadius( ): Number {
			return this.interactionRadius ;
		}

		/**
		 * Checks if two objects are interracting with each other.
		 * This happens if distance between them is smaller, then sum of their's interaction radiuses.
		 * If any of interaction radiuses are zero, no interaction allowed.
		 * This means objects are transparent to each other.
		 */
		public function isInteractedWith(object: SpaceObject): Boolean{
			if( this.getInteractionRadius() == 0 || object.getInteractionRadius() == 0)
				return false;

			var dist: Vector2d = object.getPosition().sub(this.getPosition());

			return ( (this.getInteractionRadius() + object.getInteractionRadius()) >= dist.norm);
		}

		/**
		 * Set mass of the object.
		 * @param mass New object's mass.
		 */
		public function setMass( mass: Number ): void {
			this.mass = mass;
		}

		/**
		 * Return object's mass.
		 */
		public function getMass( ): Number {
			return this.mass;
		}

		/**
		 * Set moment of inertia of the object.
		 * @param momentOfInertia New object's moment of inertia.
		 */
		public function setMomentOfInertia( momentOfInertia: Number ): void {
			this.momentOfInertia = momentOfInertia;
		}

		/**
		 * Return object's moment of inertia.
		 */
		public function getMomentOfInertia( ): Number {
			return this.momentOfInertia;
		}

		/**
		 * Set position of the object.
		 * @param position New object's position vector.
		 */
		public function setPosition( position: Vector2d ): void {
			// Updates position stored inside the object
			this.position = position;
			// Update MovieClip position
			this.x = this.position.x;
			this.y = this.position.y;
		}

		/**
		 * Return position of the object.
		 */
		public function getPosition( ): Vector2d {
			return this.position;
		}

		/**
		 * Set rotation angle of the object.
		 * @param rotationAngle New object's rotation angle.
		 */
		public function setRotationAngle( rotationAngle: Number ): void {
			this.rotationAngle = rotationAngle;
			this.rotation = Angle.radiansToDegrees(this.rotationAngle);
		}

		/**
		 * Return rotation angle of the object.
		 */
		public function getRotationAngle( ): Number {
			return this.rotationAngle;
		}

		/**
		 * Set linear velocity of the object.
		 * @param velocity New object's linear velocity.
		 */
		public function setVelocity( velocity: Vector2d ): void {
			this.velocity = velocity;
		}

		/**
		 * Return linear velocity of the object.
		 */
		public function getVelocity( ): Vector2d {
			return this.velocity;
		}

		/**
		 * Set angular velocity of the object.
		 * @param angularVelocity New object's angular velocity.
		 */
		public function setAngularVelocity( angularVelocity: Number ): void {
			this.anglularVelocity = angularVelocity;
		}

		/**
		 * Return angular velocity of the object.
		 */
		public function getAngularVelocity( ): Number {
			return this.anglularVelocity;
		}

		/**
		 * Set external force applied to the object.
		 * This force added to superposition of all other's forces and can simulate gravity.
		 * @param force External force.
		 */
		public function setExternalForce( force: Vector2d ): void {
			this.externalForce = force;
		}

		/**
		 * Return external force only applied to the object.
		 */
		public function getExternalForce( ): Vector2d {
			return this.externalForce;
		}

		/**
		 * Calculates superposition of all forces applied to the object.
		 * This is virtual method and actually every inherited class must override it
		 * adding other force sources, for example engine forces etc.
		 */
		public function getTotalForce( ): Vector2d {
			return this.getExternalForce();
		}

		/**
		 * Set external torque only applied to the object.
		 * This torque can added to other torque sources, results in angular acceleration,
		 * It can be used to simulate different side effects, for example ship enters to electrical storm.
		 */
		public function setExternalTorque( newTorque: Number ): void {
			this.externalTorque = newTorque;
		}

		/**
		 * Return external torque only applied to the object.
		 */
		public function getExternalTorque( ): Number {
			return this.externalTorque;
		}

		/**
		 * Calculates superposition of all torques applied to the object.
		 * This is virtual method and actually every inherited class must override it
		 * adding other force sources, for example engine torques etc.
		 */
		public function getTotalTorque( ): Number {
			return this.getExternalTorque();
		}

		public function setLut( lut: Lut ): void{
			if( lut == null)
				return;
			this.lut = lut.clone();
		}

		public function getLut(): Lut{
			return this.lut;
		}

		/**
		 * This method called for each object on the stage each frame,
		 * causing physical model propogate with the time.
		 */
		private function onEnterFrameListener(event: Event) {
			// only relevant for active object, connected to scene
			if(this.parent == null)
				return;

			applyPhisics( 1 / stage.frameRate );
			applyIntelegence();
		}

		/**
		 * Listener for removing from stage.
		 */
		public function onRemovedListener(e: Event): void {
			this.removeEventListener( Event.ENTER_FRAME, onEnterFrameListener );
		}

		/**
		 * This is the method, where all physical parameters of the object propogate with time.
		 * Be aware, that getTotalForce() and getTotalTorque() are virtual methods, so each
		 * object derived from SpaceObject can customize it's moving model.
		 * @param deltaT Time passed from last calculation.
		 */
		private function applyPhisics(deltaT: Number)
		{
			//F=M*A -> A=F/M
			var acceleration = this.getTotalForce().mul(1 / this.getMass());

			//V = A*deltaT + V0
			var Vnew = acceleration.mul(deltaT).add(this.getVelocity());
			//friction
			//Vnew.mul(this.getForceFriction());

			this.setVelocity( Vnew );

			//R=V*deltaT + R0
			var Rnew = this.getVelocity().mul(deltaT).add(getPosition());
			this.setPosition( Rnew );

			//Tau=I*(dw/dt)
			var angularAcceleration = this.getTotalTorque() / this.getMomentOfInertia();

			//Omega_1 = (dw/dt)*deltaT + Omega_0
			var newOmega = angularAcceleration * deltaT + this.getAngularVelocity();

			//friction
			//newOmega*=this.getTorqueFriction();
			this.setAngularVelocity( newOmega );

			//Teta2=Omega*deltaT + Teta1
			var newTeta = this.getAngularVelocity() * deltaT + this.getRotationAngle();
			this.setRotationAngle( newTeta );

			applyResources(deltaT);

		}

		/**
		 * Method, where object chooses his behaviour in the world.
		 * This is virtual method, allowing each derived class customize it's behaviour model.
		 */
		protected function applyIntelegence(): void
		{
			// empty
		}

		/**
		 * Object made a collision with other object and will be destroyed soon.
		 * It can be scrap pieces, game recources and or anything else
		 */
		public function applyCollision( other: SpaceObject ): void {
		    // empty
		}

		/**
		 * Let the other structures of the ship to update their status according to the time.
		 * For example, engine should update it's fuel as the time passes.
		 * @param deltaT Time passed from last calculation.
		 */
		public function applyResources(deltaT: Number){
			// empty
		}

		/**
		 * Returns object's health points.
		 */
		public function getHealthPoints(): Number {
		    return this.healthPoints;
		}

		/**
		 * Set health point for an object.
		 * If new health points status is below minimal allowed, object deads.
		 * Method sends request to the game class to remove him from the pippepline.
		 * @param healthPoints New health points of the object.
		 */
		public function setHealthPoints( healthPoints: Number ): void {
		    this.healthPoints = healthPoints;
		    if (healthPoints <= this.MIN_HEALTH_POINTS) {
				this.destruction();
				dispatchEvent(new GameRequest(GameRequest.removeObject, this));
		    }
		}

		/**
		 * Returns maximum allowed health points for an object.
		 */
		public function getMaxHealthPoints(): Number {
		    return this.maxHealthPoints;
		}

		/**
		 * Set maximum health point value for an object.
		 * @param healthPoints New maximal health points of the object.
		 */
		 public function setMaxHealthPoints( healthPoints: Number ): void {
		    this.maxHealthPoints = healthPoints;
		 }

		/**
		 * Do damage equals to given number of health points.
		 * If object deads, also send request to game to remove him.
		 * @param delta Health points damage value.
		 */
		public function applyDamage( delta: Number ): void {
		  this.setHealthPoints(Math.max(this.MIN_HEALTH_POINTS, this.getHealthPoints() - delta));
		}

		/**
		 * Heals object with given ammount of health points.
		 * @param delta Health points heal value.
		 */
		public function heal( delta: Number ): void {
		  this.setHealthPoints(Math.min(this.getMaxHealthPoints(), this.getHealthPoints() + delta));
		}

		/**
		 * Added to stage listener.
		 */
		private function onInitListener( e: Event ) {
		  this.addEventListener( Event.ENTER_FRAME, onEnterFrameListener );
		}

		/**
		 * Return damage value this object makes to other object in collision.
		 */
		public function getDamageValue(): Number {
		    return this.damageValue;
		}

		/**
		 * Set damage value this ship make to other object in the collision.
		 * @param damageValue New damage value.
		 */
		public function setDamageValue( damageValue: Number ): void {
		    this.damageValue = damageValue;
		}

		/**
		 * This method returns the owner of the object and can be overloaded in sub classes.
		 * Different algorithms should behave differently according to this. For example:
		 * 1. Rocket can't kill it's owner
		 * 2. If rocket attacked AI controlled ship, it must attack who shoots the rocket.
		 * By default we return null, means no owner for the object.
		 */
		public function getOwner(): SpaceObject{
			return null;
		}

		/* Starting special effects relevant to this object.
		 * For example, making scrap or generate lut.
		 */
		protected function destruction(): void{
			// empty
		}

		/* Playing animation of damage, number moving over the screen.
		 * This is only an animation, no interaction possible with this object.
		 */
		public function playDamageAnimation(value: Number): void{
            // Create damage particle only if damage is'nt zero.
			if(value == 0 || this.parent == null)
				return ;

			var size = 0.7 + Math.min(1,value / this.getMaxHealthPoints());
			var speed = this.getVelocity().normalize();
			var particle = new DamageParticle(value, size, speed);
			particle.x = this.x;
			particle.y = this.y;
			this.parent.addChild(particle);
		}

		public function playBoomAnimation( particlesCount: Number, size: Number ): void{
			if(this.parent == null)
				return;

			new Boom(this.parent, this.getPosition(), particlesCount, size);
		}

		public function playPostMortemAnimation(): void{
			if(this.parent == null)
				return;

			const deathParticle: DeathParticle = new DeathParticle(this.getPosition());
			this.parent.addChild(deathParticle);
		}

		public function createScoreObject( bonus: Number ): void{
			if(this.parent == null)
				return;

            var speed: Vector2d = this.getVelocity();
            var score: Score = new Score( bonus, this.getPosition(), this.getRotationAngle(), speed, this.getAngularVelocity());
            this.dispatchEvent(new GameRequest(GameRequest.createObject, score));
		}

		public function createShockWave( peakDamage: Number, expansionRadius: Number ): void{
			if(this.parent != null)
				this.parent.addChild(new ShockWave(this.getPosition(), peakDamage, expansionRadius));
		}

	}
}
