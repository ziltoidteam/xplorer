﻿package xplorer.game.objects {
    import xplorer.game.objects.SpaceObject;

	public class MaintenanceQueue {
		//Hold all objects in the game
		private var objects:Vector.<SpaceObject> = new Vector.<SpaceObject>();

		private var delQueue: Vector.<SpaceObject> = new Vector.<SpaceObject>();
		private var addQueue: Vector.<SpaceObject> = new Vector.<SpaceObject>();

		public function LifeQueue() {
			// constructor code
		}

		public function add(object: SpaceObject): void{
			if( object == null)
				return;

			if(objects.indexOf(object) != -1 ||
			   addQueue.indexOf(object) != -1)
				return;

			addQueue.push(object);
		}

		public function del(object: SpaceObject): void{
			delQueue.push(object);
		}

		/*
		 * This function assumes there is no duplicates int objects.
		 */
		public function sync(): void{
			// Removing
			for each(var element in delQueue){
				var index: int = objects.indexOf(element);
				if (index != -1)
					objects.splice(index,1);
			}
			// Adding
			for each(var element in addQueue){
				objects.push(element);
			}

			addQueue.splice(0,addQueue.length);
			delQueue.splice(0,addQueue.length);
		}

		public function getObjects(): Vector.<SpaceObject>{
			return objects;
		}
	}

}
