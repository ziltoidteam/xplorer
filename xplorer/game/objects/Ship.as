﻿package xplorer.game.objects {
    import xplorer.parts.engines.Engine;
    import xplorer.parts.weapons.WeaponObject;
    import xplorer.system.Vector2d;
    import xplorer.AI.BaseAI;
	import xplorer.system.Random;
    import xplorer.game.objects.SpaceObject;
    import xplorer.game.objects.FlyingObject;
    import xplorer.game.Lut;

    /**
     * This class is base for all ship objects.
     * It extends FlyingObject with Intelect and weapon system.
     */
    public class Ship extends FlyingObject {
	    var intelegence = null;
		var weapons:Vector.<WeaponObject> = new Vector.<WeaponObject>();

		const SHIP_IRADIUS: Number = 20; //TODO: will be taken from ship model system

		/**
		 * Constructor.
		 */
		public function Ship(objectMass: Number, objectMomentOfInertia: Number,
				     position: Vector2d, rotationAngle: Number, velocity: Vector2d, angularVelocity: Number,
				     externalForce: Vector2d, externalTorque: Number,
				     damageValue: Number, healthPoints: Number,  maxHealthPoints: Number, lut: Lut,
				     engine: Engine, intelegence: BaseAI ) {
			super(objectMass, objectMomentOfInertia, position, rotationAngle, velocity, angularVelocity,
				  externalForce, externalTorque, SHIP_IRADIUS, damageValue, healthPoints,  maxHealthPoints, lut, engine);
			setIntelegence(intelegence);
		}

		/**
		 * Cloning method.
		 */
		override public function clone(): SpaceObject {
			var ship: Ship = new Ship(getMass(), getMomentOfInertia(), getPosition(),
                getRotationAngle(), getVelocity(), getAngularVelocity(), getExternalForce(),
                getExternalTorque(), getDamageValue(), getHealthPoints(), getMaxHealthPoints(),
                getLut(), getEngine(), getIntelegence());

			for each (var weapon: WeaponObject in weapons) {
				ship.addWeapon(weapon);
            }

			return ship;
		}

		/**
		 * Apply collision for ships.
		 * Making damage to the ship each time it interacts with other object.
		 */
		override public function applyCollision( other: SpaceObject ): void {
			/* Tell AI it was attacked by some object.
			 * 3 different scenario are possible.
			 * Owner is null - attack should be ignored, for example, collision with asteroid
			 * Owner is object itself - attack the object, for example collision with other ship.
			 * Owner is other object - attack the owner, for example, for rocket it will be ship who launched it.
			 */
			if(getIntelegence() != null)
				this.getIntelegence().addEnemy(other.getOwner());
            var damageValue = other.getDamageValue();
			this.playDamageAnimation(damageValue);

			// Apply damage
		    this.applyDamage(damageValue);
		}

		/**
		 * Adds weapon to the ship.
		 */
        public function addWeapon(weapon: WeaponObject): void{
            if(weapon == null)
                return;
            weapons.push(weapon.clone());
        }

        public function setWeapon(weapon: WeaponObject): void{
            if(weapon == null)
                return;

            weapons = new Vector.<WeaponObject>();
            weapons.push(weapon.clone());
        }

		/**
		 * Shoots a weapon.
		 * @parm index The index of the weapon as it has beens added to the ship.
		 */
		public function shootWeapon(index: Number): void{
			if(index >= 0 && index < weapons.length)
				weapons[index].shoot(this);
		}

        public function getActiveWeapon(): WeaponObject {
            if (weapons.length < 0)
                throw "getActiveWeapon: no weapons";

            return weapons[0];
        }

		/**
		 * Controlling the ship with the intelect.
		 */
		override protected function applyIntelegence(): void
		{
			if( intelegence == null ) return;
			intelegence.act(this);
		}

		/**
		 * Sets ship's intelegence module.
		 * @parm intelegence New intelegence module.
		 */
		public function setIntelegence( intelegence: BaseAI ): void{
			if( intelegence == null )
				this.intelegence = null;
			else
				this.intelegence = intelegence.clone();
		}

		/**
		 * Returns ship's intelegence module.
		 */
		public function getIntelegence(): BaseAI{
			return this.intelegence;
		}

		/**
		 * Owner object of all the ships are the ship itself.
		 */
		override public function getOwner(): SpaceObject{
			return this;
		}

        override protected function destruction(): void {
			this.playBoomAnimation(30,0.9);
			this.playPostMortemAnimation();

			const bonus: Number = Random.randomRange(1,100); // Fix: connect it to ship level
            this.createScoreObject( bonus );
        }

		override public function applyDamage( delta: Number ): void {
		  super.applyDamage( delta );
		  this.playDamageAnimation(delta);
		}

		/**
		 * Te idea of reset is to clear all geometric/phisical propertioes of the ship and refill all
		 * ammo, fuel etc to maxmum value.
		 */
		public function reset(): void{
		    this.setHealthPoints(this.getMaxHealthPoints());
            this.setVelocity(new Vector2d(0, 0));
            this.setAngularVelocity(0);
            this.setPosition(new Vector2d(0, 0));
            this.getEngine().setFuel(this.getEngine().getMaxFuel());
			for each(var weapon in this.weapons)
				weapon.setQuantity(weapon.getMaxQuantity());
		}
    }
}
