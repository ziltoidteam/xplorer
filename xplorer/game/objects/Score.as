﻿package xplorer.game.objects {
    import xplorer.parts.engines.Engine;
    import xplorer.system.Vector2d;
	import flash.display.MovieClip;
    import xplorer.game.objects.SpaceObject;
    import xplorer.game.objects.FlyingObject;
    import xplorer.game.Lut;

	public class Score extends FlyingObject {

		public function Score(score: Number, position: Vector2d, rotationAngle: Number, velocity: Vector2d, angularVelocity: Number) {
			var objectMass: Number = 1;
            var objectMomentOfInertia: Number = 2;
			var externalForce: Vector2d = new Vector2d(0, 0);
            var externalTorque: Number = 0;
			var interactionRadius: Number = (this.width + this.height) / 6; // Stupid fix
            var damageValue: Number = 0;
			var healthPoints: Number = 1;
            var maxHealthPoints: Number = 1;
			var lut: Lut = new Lut();

            var engine: Engine = new Engine(0, 0 , 0, 0, 0.015, 0.015, 0, 0);

			super(objectMass, objectMomentOfInertia, position, rotationAngle, velocity, angularVelocity,
                  externalForce, externalTorque, interactionRadius, damageValue, healthPoints,
                  maxHealthPoints, lut, engine);

			this.setScore(score);
		}

		override public function applyCollision( other: SpaceObject ): void {
			/* If some other ship interracts with score object,
			 * add bonuses to the object it interacts with */
            if (other.getOwner() == null) return;

			// Get Lut of this score, score itself to the object collected it.
			other.getOwner().getLut().add(this.getLut());

			// Destroy the score
			this.setHealthPoints(0);
		}

		/**
		 * Cloning function
		 */
		override public function clone(): SpaceObject{
				return new Score(this.getScore(), getPosition(), getRotationAngle(),
								 getVelocity(), getAngularVelocity());

		}

		public function getScore(): Number{
			return this.getLut().getScore();
		}

		public function setScore(score: Number):void {
			this.getLut().setScore( score );
			this.score.text = getScoreAsText();
		}

		private function getScoreAsText(): String{
			var score: int = this.getLut().getScore();
            var textValue: String = String(score);

			return textValue;
		}

	}

}
