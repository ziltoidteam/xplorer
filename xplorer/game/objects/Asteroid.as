﻿package xplorer.game.objects {
    import xplorer.system.Random;
    import xplorer.system.Vector2d;
    import xplorer.game.objects.SpaceObject;
    import xplorer.game.requests.GameRequest;
    import xplorer.game.Lut;

	public class Asteroid extends SpaceObject{

		var edges:Vector.<Vector2d> = new Vector.<Vector2d>();

		public function Asteroid(objectMass: Number, objectMomentOfInertia: Number,
									position: Vector2d, rotationAngle: Number, velocity: Vector2d, angularVelocity: Number,
									externalForce: Vector2d, externalTorque: Number,
									damageValue: Number, healthPoints: Number,  maxHealthPoints: Number, lut: Lut) {
			 super(objectMass, objectMomentOfInertia, position, rotationAngle, velocity, angularVelocity,
				   externalForce, externalTorque, 0, damageValue, healthPoints,  maxHealthPoints, lut);
		}

		override public function clone(): SpaceObject{
			var asteroid: Asteroid = new Asteroid(getMass(), getMomentOfInertia(), getPosition(), getRotationAngle(),
												  getVelocity(), getAngularVelocity(), getExternalForce(), getExternalTorque(),
												  getDamageValue(), getHealthPoints(), getMaxHealthPoints(), getLut());

			asteroid.setInteractionRadius(this.getInteractionRadius());
			for each(var edge in this.edges){
				asteroid.edges.push(edge);
			}
			return asteroid;
		}

		/**
		 * Authomaticly creates random asteroid in a given range of parameters.
		 * @param maxFrindges The bigger the number, more frindges to the asteroid. 20 would be good.
		 * @param minRadius Minimum radius of the random asteroid. 20 would be good.
		 * @param maxRadius Maximum radius of the random asteroid. 50 would be good.
		 */
		public function initRandom(maxFrindges: Number, minRadius: Number, maxRadius: Number){
            const asteroidRadius: Number = Random.randomRange(minRadius, maxRadius);
            const frindges = Random.randomRange(maxFrindges/2,maxFrindges);

			var edges:Vector.<Vector2d> = new Vector.<Vector2d>();

    		for (var index:Number=0; index<frindges; ++index){
                const radius: Number = Random.randomRange(asteroidRadius/2, asteroidRadius);
                const angle: Number = index * (2*Math.PI/frindges);

                const vec: Vector2d = Vector2d.fromPolar(radius, angle);
                edges.push(vec);
    		}

			initWithEdges(edges);
		}

		/**
		 * Creates custom asteroid from given vertexes.
		 * This functions also uses vertexes data to calculate mass, interaction
		 * radius and damage of newly made asteroid.
		 * @param edges Vector of asteroid's vertexes
		 */
		public function initWithEdges(edges:Vector.<Vector2d>): void{

			// Copy the edges to internal vector
			this.edges = new Vector.<Vector2d>();
			for each(var edge in edges)
				this.edges.push(edge);

			// Center of mass correction
			const centerOfMass: Vector2d = getMeanVector(this.edges);
			for each(var element in this.edges)
				element = element.sub(centerOfMass);

			// Connect last vertex with first one
			if(this.edges.length > 1)
				this.edges.push(this.edges[0]);

			// Find max distance for setting interraction radius
			var asteroidRadius: Number = 0;
			for each(var edge in this.edges)
				if(edge.norm > asteroidRadius)
					asteroidRadius = edge.norm;
			this.setInteractionRadius(asteroidRadius);
			/* TODO: in addition to calcultaion of interaction radius, we can calculate
				damage, and health parameters here, releasing user from setting them in constructor.
			 */
			// We use simpliest model, when mass and damage calculated linearly:
			this.setMass(asteroidRadius); // Mass = 1*R, when M=1 is assumed to be the scale of ship masses.
			this.setDamageValue(asteroidRadius); // Damage = 1*R, when R=20-50 is the scale of asteroid radiuses and 100 is the scales of ship healtpoints

			this.graphics.clear();
			this.graphics.lineStyle(2, 0xCCCCCC);
			for(var index:Number = 0; index < this.edges.length - 1; ++index){
				 this.graphics.moveTo(this.edges[index+0].x,this.edges[index+0].y);
				 this.graphics.lineTo(this.edges[index+1].x,this.edges[index+1].y);
			}
			this.graphics.endFill();
		}

		/**
		 * Collision handler for asteroids.
		 * By now, it replaces this asteroid by some newly transparent smaller asteroids.
		 * @parm other The obect asteroid interacted with.
		 */
		override public function applyCollision( other: SpaceObject ): void {
			this.applyDamage(other.getDamageValue());
		}

		/**
		 * For all asteroid's geometry, the simpliest method for center of mass calculation is
		 * geometric mean of vectors.
		 */
		private static function getMeanVector( vectors: Vector.<Vector2d>): Vector2d{
			var result: Vector2d = new Vector2d(0,0);

			if( vectors == null || (vectors.length == 0)) return result;

			for each(var element in vectors)
				result = result.add(element);

			return result.mul( 1 / vectors.length );
		}

		/** Starting special effects relevant to this object.
		  * For example, making scrap or generate lut.
		  * In this specific case of asteroid, we scrapping this
		  * asteroid to couple of little asteroids.
		  */
		override protected function destruction(): void{
				/* If object would be deleted soon, scrap it to pieces
				 */
				var asteroids: Vector.<SpaceObject> = new Vector.<SpaceObject>();

				var middlePoint: Vector2d = new Vector2d(0,0);
				var numberOfScraps: Number = Random.randomRange(2,4);
				var vertexesPerScrap: Number = this.edges.length / numberOfScraps;

				for(var index = 0; index < this.edges.length; index+=vertexesPerScrap)
				{
					// TODO:  make some complex physics calculations on asteroids
					// remove magic numbers from speed and do some randomization.
					var scrapEdges = this.edges.slice(index,Math.min(this.edges.length-1, index + vertexesPerScrap));
					scrapEdges.push(middlePoint);

					const M: Number = 0;// Will be recalculated in initWithEdges()
					const I: Number = this.getInteractionRadius()/numberOfScraps;
					const R: Vector2d = getMeanVector(scrapEdges).add(this.getPosition());
					const angle: Number = 0;
					const V: Vector2d = new Vector2d(Random.randomRange(-10,10),Random.randomRange(-10,10));
					const omega: Number = Random.randomRange(-1,1);
					const extF = new Vector2d(0,0);
					const extT = new Vector2d(0,0);
					const damage: Number = 0; // Will be recalculated in initWithEdges()
					const maxHealth: Number = 100;
					const lut: Lut = new Lut();

					var asteroid: Asteroid = new Asteroid(M, I, R, angle, V, omega, extF, extT, damage, maxHealth, maxHealth, lut);
					asteroid.initWithEdges(scrapEdges);
				// Newly created asteroid will not interact
					asteroid.setInteractionRadius(0);
					asteroid.alpha = 0.5; // Temporary effect

					dispatchEvent(new GameRequest(GameRequest.createObject, asteroid));
				}
		}

	}

}
