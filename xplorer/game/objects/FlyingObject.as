﻿package xplorer.game.objects {
    import xplorer.parts.engines.Engine;
    import xplorer.system.Vector2d;
    import xplorer.game.objects.SpaceObject;
    import xplorer.game.Lut;

	public class FlyingObject extends SpaceObject {
        var engine: Engine;

		/**
		 * Constructor
		 */
		public function FlyingObject(objectMass: Number, objectMomentOfInertia: Number,
									position: Vector2d, rotationAngle: Number, velocity: Vector2d, angularVelocity: Number,
									externalForce: Vector2d, externalTorque: Number,
									interactionRadius: Number, damageValue: Number,
									healthPoints: Number,  maxHealthPoints: Number, lut: Lut, engine: Engine ) {
			super(objectMass, objectMomentOfInertia, position, rotationAngle, velocity, angularVelocity,
				  externalForce, externalTorque, interactionRadius, damageValue, healthPoints,  maxHealthPoints, lut);
			setEngine(engine);
		}

		/**
		 * Cloning function
		 */
		override public function clone(): SpaceObject{
				return new FlyingObject(getMass(), getMomentOfInertia(), getPosition(), getRotationAngle(),
										getVelocity(), getAngularVelocity(), getExternalForce(), getExternalTorque(),
										getInteractionRadius(), getDamageValue(), getHealthPoints(), getMaxHealthPoints(), getLut(), getEngine());

		}

		/**
		 *  Gets superposition of all forses on the object.
		 *  1. The world force
		 *  2. Engine force
		 *  3. Friction like powers in the form of -f*v^2 for stabilization.
		 */
		override public function getTotalForce(): Vector2d { // virtual!!!
			// External force
			var superposition: Vector2d = this.getExternalForce();
			if (this.engine == null)
				return superposition;

			// Engine friction
			var absFriction = engine.getForceFriction() * Math.pow(this.getVelocity().norm,2);
			var friction = this.getVelocity().normalize().mul(-absFriction);
			// Engine power
			var engineForce = Vector2d.fromPolar(engine.getForce(), this.getRotationAngle() + Math.PI / 2);

			superposition = superposition.add(friction).add(engineForce);
			return superposition;
		}

		/**
		 *  Gets superposition of all torques on the object.
		 *  1. The world torque
		 *  2. Engine torque
		 *  3. Friction like torque in the form of -f*w^2 for stabilization.
		 */
		override public function getTotalTorque(): Number { // virtual!!!
			// External torque
			var superposition = this.getExternalTorque();

			if (this.engine == null)
				return superposition;

			//Engine torque
			superposition += engine.getTorque();

			// Engine friction
			var absFriction = engine.getTorqueFriction() * Math.pow(this.getAngularVelocity(),2);
			if(this.getAngularVelocity() >= 0)
					absFriction *= -1;
				superposition += absFriction;

			return superposition;
		}

		override public function applyResources(deltaT: Number){
			if (this.engine) getEngine().useFuel(deltaT);
		}

		public function setEngine(engine: Engine): void{
			if(engine != null)
				this.engine = engine.clone() as Engine;
			else
				this.engine = null;
		}

		public function getEngine(): Engine {
			return engine;
		}

	}

}
