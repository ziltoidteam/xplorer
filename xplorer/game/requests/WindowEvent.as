﻿package xplorer.game.requests {
	import flash.events.Event;

	/**
	 *  This class makes possible for various objects to querry game and respond accordingly.
	 */
	public class WindowEvent extends Event
	{
		public static const successInMission: String = "successInMissionEvent";	// request to add an object to the game.
		public static const failInMission: String = "failInMissionEvent";	// request to add an object to the game.

		public function WindowEvent(type: String ){
			super(type, true, false);
		}

		/**
		 * Makes an event cloning.
		 */
		override public function clone():Event{
			return new WindowEvent(this.type);
		}

	}
}
