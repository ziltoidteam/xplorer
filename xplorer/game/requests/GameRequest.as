﻿package xplorer.game.requests {
	import flash.events.Event;
    import xplorer.game.objects.SpaceObject;

	/**
	 *  This class makes possible for various objects to querry game and respond accordingly.
	 */
	public class GameRequest extends Event
	{
		public static const createObject: String = "createObjectRequest";	// request to add an object to the game.
        public static const removeObject: String = "removeObjectRequest";   // request to remove object from the game.
		public static const searchNearObjects: String = "searchNearObjects";// request from game for nearest to caller objects.
		public static const checkIfDead: String = "checkIfDeadRequest";     // request from a game to check if object is not valid (not on the scene, probably dead).
		public static const getPlayerShip: String = "getPlayerShipRequest";     // Request from a game to get a player;

		private var caller: SpaceObject;
		private var backMethod: Function = null;

		/**
		 * Initializes an request.
		 * @param caller Object who makes a request.
		 * @param backMethod Caller's method who should be called to return game's respondings to caller.
		 */
		public function GameRequest(type:String, caller: SpaceObject, backMethod: Function = null ){
			super(type, true, false);
			this.caller = caller;
			this.backMethod = backMethod;
		}

		/**
		 * Get method should be called to communicate with the caller.
		 * Not all requests need this method, in this case it will be ignored.
		 */
		public function getBackMethod(): Function{
			return this.backMethod;
		}

		/**
		 * Get object who make makes a request.
		 */
		public function getCaller(): SpaceObject{
			return this.caller;
		}

		/**
		 * Makes an event cloning.
		 */
		override public function clone():Event{
			return new GameRequest(this.type, getCaller(), getBackMethod());
		}

	}
}
