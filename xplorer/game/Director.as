﻿package xplorer.game {
	import flash.events.Event;
	import xplorer.system.Random;
	import xplorer.AI.BaseAI;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import xplorer.AI.TravellerAI;
	import xplorer.AI.PirateAI;
	import flash.display.MovieClip;
	import flash.geom.ColorTransform;
    import xplorer.system.Debug;
    import xplorer.parts.engines.*;
    import xplorer.parts.weapons.*;
    import xplorer.system.Vector2d;
    import xplorer.game.objects.Asteroid;
    import xplorer.game.objects.SpaceObject;
    import xplorer.game.requests.GameRequest;
    import xplorer.system.Range;
    import xplorer.parts.weapons.shells.*;
    import xplorer.game.Lut;
    import xplorer.game.objects.Ship;

	/**
	 * Class to handle sawn director for the game.
	 * It dynamically responds to players actions, making player's life
	 * easier or harder with various events like enemy generations, asteroids etc.
	 */
	public class Director extends MovieClip {
		private var enemyTimer: Timer = new Timer(9321);	// Timer for enemy handler
		private var asteroidTimer: Timer = new Timer(3532);	// Timer for asteroid handler
		private var correctionTimer: Timer = new Timer(10000);  // Timer to recalculate the situation in the game to change game level.

		private var currentLevel: Number = 0;		// Enemy level. This is the number from 0 to 100
		private var minLevel: Number = 0;
		private var maxLevel: Number = 0;
		private var maxObjects: Number = 0;

		private var playerShip: Ship = null;

		public function Director(maxObjects: Number, minLevel: Number, maxLevel: Number) {
			// Initialize event listeners
			enemyTimer.addEventListener(TimerEvent.TIMER, onEnemyTimer);
			asteroidTimer.addEventListener(TimerEvent.TIMER, onAsteroidTimer);
			correctionTimer.addEventListener(TimerEvent.TIMER, onCorrectionTimer);

			this.addEventListener( Event.ADDED_TO_STAGE, onInitListener );
			this.addEventListener( Event.REMOVED_FROM_STAGE, onRemovedListener );

			this.minLevel = minLevel;
			this.maxLevel = maxLevel;
			this.currentLevel = minLevel;
			this.maxObjects = maxObjects;
		}

		/**
		 * Runs each time director added to stage to request to initialize internal variables.
		 */
		private function onInitListener( e: Event ) {
			// Request game for a player ship.
			dispatchEvent(new GameRequest(GameRequest.getPlayerShip, null, setPlayerShip));

			// Start timers
			enemyTimer.start();
			asteroidTimer.start();
			correctionTimer.start();
		}

		/**
		 * Runs each time director removed from stage.
		 * If you will not do it, timers will continue to run when
		 * player enters game menu.
		 */
		private function onRemovedListener( e: Event ) {
			enemyTimer.stop();
			asteroidTimer.stop();
			correctionTimer.stop();
		}

		/**
		 * Set internal variable to hold player ship object.
		 * It can be any ship, director class is focused on, not actual player.
		 * @param playerShip ship for sawn director to focus on.
		 */
		private function setPlayerShip(playerShip: Ship): void{
			this.playerShip = playerShip;
		}

		/*
		 * In this class we commonly use level parameter in functions.
		 * We do this to generate different objects of given level.
		 * For level X, all object parameters are randomly generated in range [0,X].
		 * If for some parameter, 0 is the most valuable value, then [100-X,100] is taken.
		 * Level 0 is converted to smaller possible value for parameter, 100 to biggest
		 * one and for some middle value interpolated linearly in this range.
		 * Be aware: random is not uniform, look Range class for details.
		 */

		/**
		 * Generate ship according to given level.
		 * @param level a positive number from 0 to 100.
		 */
		static public function generateShip(level: Number): Ship{
			const R: Vector2d = new Vector2d(0,0);
			const angle: Number = 0;
			const V: Vector2d = new Vector2d(0,0);
			const omega: Number = 0;
			const extF = new Vector2d(0,0);
			const extT = 0;

			const M: Range = new Range(1 - 0.5, 1 + 0.5);
			const I: Range = new Range(2 - 0.7, 2 + 0.7);
			const damage: Range = new Range(10 - 5, 10 + 5);
			const maxHealth: Range = new Range(100 - 60, 100 + 60);
			const maxHealthValue = maxHealth.random(level);

			const lut: Lut = new Lut();

			const engine: Engine = getShipEngine(level);
			var weapon: WeaponObject;
            var r = Random.randomRange(0, 100);
            if (r <= 33)
                weapon = getMachineGun(level);
            else if (r <= 66)
                weapon = getRocketLauncher(level);
            else
                weapon = getMineShooter(level);

			const intelegence: BaseAI = null;

			var ship: Ship = new Ship(M.random(level),
                                      I.random(level),
                                      R,
                                      angle,
                                      V,
                                      omega,
                                      extF,
                                      extT,
                                      damage.random(level),
                                      maxHealthValue,
                                      maxHealthValue,
									  lut,
                                      engine,
                                      intelegence);

			ship.addWeapon(weapon);

			return ship;
		}

        static public function getRocket(level: Number): Rocket{
            const R: Vector2d = new Vector2d(0,0);
            const angle: Number = 0;
            const V: Vector2d = new Vector2d(0,0);
            const omega: Number = 0;
            const extF = new Vector2d(0,0);
            const extT = 0;

            const M: Range = new Range(0.05 - 0.03, 0.05 + 0.03);
            const I: Range = new Range(0.5 - 0.2, 0.5 + 0.2);
            const damage: Range = new Range(10 - 5, 10 + 5);
            const maxHealth: Range = new Range(30 - 10, 30 + 10);
            const maxHealthValue: Number =  maxHealth.random(level);

			const lut: Lut = new Lut();

            const engine: Engine = getRocketEngine(level);

            return new Rocket(M.random(level),
                              I.random(level),
                              R,
                              angle,
                              V,
                              omega,
                              extF,
                              extT,
                              damage.random(level),
                              maxHealthValue,
                              maxHealthValue,
							  lut,
                              engine,
                              null,
                              null);
        }

        static public function getMine(level: Number): Mine{
            const R: Vector2d = new Vector2d(0,0);
            const angle: Number = 0;
            const V: Vector2d = new Vector2d(0,0);
            const omega: Number = 0;
            const extF = new Vector2d(0,0);
            const extT = 0;

            const M: Range = new Range(0.05 - 0.03, 0.05 + 0.03);
            const I: Range = new Range(0.5 - 0.2, 0.5 + 0.2);
            const damage: Range = new Range(50 - 30, 50 + 30);
            const maxHealth: Range = new Range(30 - 10, 30 + 10);
            const maxHealthValue: Number =  maxHealth.random(level);

			const lut: Lut = new Lut();

			const shockWavePeakDamage = new Range(15,50);
			const shockWaveExpansionRadius = new Range(10,60);

            return new Mine(M.random(level),
                              I.random(level),
                              R,
                              angle,
                              V,
                              omega,
                              extF,
                              extT,
                              0, // there is no damage with collision, only with shock wave
                              maxHealthValue,
                              maxHealthValue,
							  lut,
                              null,
							  shockWavePeakDamage.random(level),    // The only damage is thruout shock wave
							  shockWaveExpansionRadius.random(level)); //
        }

        static public function getBullet(level: Number): Bullet {
            const R: Vector2d = new Vector2d(0,0);
            const angle: Number = 0;
            const V: Vector2d = new Vector2d(0,0);
            const omega: Number = 0;
            const extF = new Vector2d(0,0);
            const extT = 0;

            const M: Range = new Range(0.05 - 0.03, 0.05 + 0.03);
            const I: Range = new Range(0.5 - 0.2, 0.5 + 0.2);
            const damage: Range = new Range(4 - 3, 4 + 3);
            const maxHealth: Range = new Range(30 - 10, 30 + 10);
            const maxHealthValue: Number =  maxHealth.random(level);

            return new Bullet(M.random(level),
                              I.random(level),
                              R,
                              angle,
                              V,
                              omega,
                              extF,
                              extT,
                              damage.random(level),
                              maxHealthValue,
                              maxHealthValue,
                              null);
        }

        static public function getRocketLauncher(level: Number): RocketLauncher{
            const respawnTime: Range = new Range(1000 - 900, 1000 + 900);
            const quantity: Range = new Range(50 - 30, 50 + 30);

            const rocket: Rocket = getRocket(level);

            return new RocketLauncher(respawnTime.randomInverse(level),
                                      rocket,
                                      quantity.random(level));
        }

        static public function getMachineGun(level: Number): MachineGun {
            const respawnTime: Range = new Range(30, 100);
            const quantity: Range = new Range(500 - 300, 500 + 300);
            const power: Range = new Range(200, 600);

            const bullet: Bullet = getBullet(level);

            return new MachineGun(respawnTime.randomInverse(level),
                                      bullet,
                                      quantity.random(level),
                                        power.random(level));
        }

        static public function getMineShooter(level: Number): MineShooter {
            const respawnTime: Range = new Range(500, 1000);
            const quantity: Range = new Range(50 - 30, 50 + 30);
            const power: Range = new Range(300, 1000);

            const mine: Mine = getMine(level);

            return new MineShooter(respawnTime.randomInverse(level),
                                      mine,
                                      quantity.random(level),
                                        power.random(level));
        }

		static public function getRocketEngine(level: Number): Engine{
			//0, 180, 0, 10, 0.0015, 1.10, 10, 10
			const force: Number = 0;
			const forceMax : Range = new Range( 180 - 30, 180 + 30 );
			const torque: Number = 0;
			const torqueMax: Range = new Range( 10 - 5, 10 + 5);
			const forceFriction: Range = new Range(0.0010 - 0.0005, 0.0010 + 0.0005);
			const torqueFriction: Range = new Range(1.10 - 0.5, 1.10 + 0.5)
			const fuelMax: Range = new Range(10 - 5,10 + 5);
			const fuelMaxValue: Number = fuelMax.random(level);

			return new Engine(force,
					  forceMax.random(level),
					  torque,
					  torqueMax.random(level),
					  forceFriction.randomInverse(level),
					  torqueFriction.randomInverse(level),
					  fuelMaxValue,
					  fuelMaxValue);

		}

        static public function getShipEngine(level: Number): Engine{
            //0, 400, 0, 20, 0.015, 1.90, 100, 100
            const force: Number = 0;
            const forceMax : Range = new Range( 400 - 30, 400 + 30 );
            const torque: Number = 0;
            const torqueMax: Range = new Range( 20 - 10, 20 + 10);
            const forceFriction: Range = new Range(0.015 - 0.012, 0.015 + 0.012);
            const torqueFriction: Range = new Range(1.90 - 0.5, 1.90 + 0.5)
            const fuelMax: Range = new Range(100 - 50,100 + 50);
            const fuelMaxValue: Number = fuelMax.random(level);

            return new Engine(force,
                              forceMax.random(level),
                              torque,
                              torqueMax.random(level),
                              forceFriction.randomInverse(level),
                              torqueFriction.randomInverse(level),
                              fuelMaxValue,
                              fuelMaxValue);

        }

        private function onEnemyTimer(e:TimerEvent): void {
            placeEnemy();
        }

        private function onAsteroidTimer(e:TimerEvent): void {
            placeAsteroid();
        }

        private function onCorrectionTimer(e:TimerEvent): void {
            correctSituation();
        }

        private function correctSituation(): void {
			if(playerShip == null)
				return;
			// How to estimate if we want to make player's life easier orharder;

			// 1) Control enemies number.
			// This is the max number of objects around player. If more, stop generate more.

			this.dispatchEvent(new GameRequest(GameRequest.searchNearObjects, playerShip, this.controlObjectsNumber));

			// 2) Control enemies level
			const healthness: Number = playerShip.getHealthPoints() / playerShip.getMaxHealthPoints();
			if(healthness < 0.5)
				currentLevel -= 5;
			else
				currentLevel += 5;

			currentLevel = Math.min(maxLevel,currentLevel);
			currentLevel = Math.max(minLevel,currentLevel);

			Debug.out("Current level: ", currentLevel);
        }

		private function controlObjectsNumber(objects: Vector.<SpaceObject>): void{
			const count: Number = 0;
			for each(var object in objects)
				if(object.getOwner() != playerShip)
					count++;
			trace(count);

			if(count > maxObjects){
				trace("Too many objects. Stop generate more");
				if(enemyTimer.running) enemyTimer.stop();
				if(asteroidTimer.running) asteroidTimer.stop();
			}
			else{
				trace("Need more objects. Start generate more");
				if(!enemyTimer.running) enemyTimer.start();
				if(!asteroidTimer.running) asteroidTimer.start();
			}


		}

        private function placeAsteroid(): void {
			if(playerShip == null)
				return;

			const M: Number = 0; // Calculated according to size
			const I: Number = Random.randomRange(1,50);
			var playerPosition: Vector2d = playerShip.getPosition();
			var deltaP: Vector2d = Vector2d.fromPolar(500, Random.randomRange(-Math.PI, +Math.PI));
			const R = playerPosition.add(deltaP);
			const angle: Number = 0;
			const V: Vector2d = playerPosition.sub(R).normalize().mul(100).rotate(Random.randomRange(-0.5,+0.5)); //TODO: magic numbers.
			const omega: Number = Random.randomRange(-2,2);
			const extF = new Vector2d(0,0);
			const extT = 0;

			const damage: Number = 0; // Will be recalculated in initWithEdges()
			const maxHealth: Number = 100;

			const lut: Lut = new Lut();

			var asteroid: Asteroid = new Asteroid(M, I, R, angle, V, omega, extF, extT, damage, maxHealth, maxHealth, lut);
			asteroid.initRandom(20,20,50);

			dispatchEvent(new GameRequest(GameRequest.createObject, asteroid));
		}

		/**
		 * Place enemy not far from player ship.
		 * Player ship is the local object stored in director and not the actual player in the game.
		 */
		public function placeEnemy() {
			if(playerShip == null)
				return;

            var enemyShip = generateShip(currentLevel);
            var playerPosition: Vector2d = playerShip.getPosition();
            var deltaP: Vector2d = Vector2d.fromPolar(500, Random.randomRange(-Math.PI, +Math.PI));
            enemyShip.setPosition(playerPosition.add(deltaP));

			var inteligence:BaseAI = null;
			if (Random.randomRange(0,100) <= 30) //30 percents
				inteligence = new PirateAI();
			else
				inteligence = new TravellerAI();
            enemyShip.setIntelegence(inteligence);

            var transformation = new ColorTransform();
            transformation.color = 0xCCCCCC;
            enemyShip.transform.colorTransform = transformation;

			dispatchEvent(new GameRequest(GameRequest.createObject, enemyShip));
		}
	}

}
