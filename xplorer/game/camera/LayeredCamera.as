﻿package xplorer.game.camera {
    import flash.display.MovieClip;
    import xplorer.system.Vector2d;

	public class LayeredCamera extends Camera {
        private var layers: Object = null;
        private var factors: Object = null;

        public function LayeredCamera( layers: Object, factors: Object ) {
			super(getMainLayer(layers, factors));
            this.layers = layers;
            this.factors = factors;
            this.setPosition(new Vector2d(0, 0));
            this.setScale(100);
        }

        // Low-level parts
        private function updateLayerPosition( layer: MovieClip, factor: Number ): void {
			if (layer == null || layer.stage == null) return;
            // reversing
            var reverseX: Number = -this.getPosition().x;
            var reverseY: Number = -this.getPosition().y;

            // centrate
            var center: Vector2d = new Vector2d( layer.stage.stageWidth / 2 / layer.scaleX, layer.stage.stageHeight / 2 / layer.scaleY );
            var centratedX: Number = reverseX + center.x;
            var centratedY: Number = reverseY + center.y;

            // setup a new coordinates
            layer.x = centratedX * layer.scaleX * factor;
            layer.y = centratedY * layer.scaleY * factor;
        }

        protected function updateLayerScale( layer: MovieClip, newScale: Number ): void {
            if (layer == null || layer.stage == null) return;
            // from percents to factor
            var factorScale: Number = newScale / 100;

            // setup a new scale
            layer.scaleX = factorScale;
            layer.scaleY = factorScale;

            // updating parent position because its changes by scale
            updateParentPosition();
        }

        override protected function updateParentScale( newScale: Number ): void {
            for (var layerName in layers) {
                var factor: Number = factors[layerName];
                var layer: MovieClip = layers[layerName];

                if (factor != 0)
                    updateLayerScale(layer, newScale);
            }
        }

        override protected function updateParentPosition(): void {
            for (var layerName in layers) {
                var factor: Number = factors[layerName];
                var layer: MovieClip = layers[layerName];

                updateLayerPosition(layer, factor);
            }
        }

        private function getMainLayer( layers: Object, factors: Object ): MovieClip {
            var mainLayer: MovieClip = null;
            var maxFactor: Number = 0;

            for (var key: String in layers) {
                if (factors[key] >= maxFactor) {
                    mainLayer = layers[key];
                    maxFactor = factors[key];
                }
            }

            return mainLayer;
        }
	}
}
