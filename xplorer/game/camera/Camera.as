﻿package xplorer.game.camera {
    import flash.display.MovieClip;
    import flash.events.Event;
    import xplorer.system.Vector2d;
    import xplorer.game.objects.SpaceObject;

	public class Camera {
        // TODO: vars -> properties
        protected var parent: MovieClip = null;
        protected var position: Vector2d = null;  // in pair of units
        protected var scale: Number = 100;        // in percents
        protected const maxScale: Number = 500;
        protected const minScale: Number = 5;

        public function Camera( parent: MovieClip ) {
            this.parent = parent; // TODO: Set parent
            this.setPosition(new Vector2d(0, 0));
            this.setScale(100);
        }

        // Low-level parts
        protected function updateParentPosition(): void {
			if (this.parent == null || this.parent.stage == null) return;
            // reversing
            var reverseX: Number = -this.getPosition().x;
            var reverseY: Number = -this.getPosition().y;

            // centrate
            var center: Vector2d = new Vector2d( this.parent.stage.stageWidth / 2 / this.parent.scaleX, this.parent.stage.stageHeight / 2 / this.parent.scaleY );
            var centratedX: Number = reverseX + center.x;
            var centratedY: Number = reverseY + center.y;

            // setup a new coordinates
            this.parent.x = centratedX * this.parent.scaleX;
            this.parent.y = centratedY * this.parent.scaleY;
        }

        protected function updateParentScale( newScale: Number ): void {
			if (this.parent == null || this.parent.stage == null) return;
            // from percents to factor
            var factorScale: Number = newScale / 100;

            // setup a new scale
            this.parent.scaleX = factorScale;
            this.parent.scaleY = factorScale;

            // updating parent position because its changes by scale
            updateParentPosition();
        }

        // position part
        public function setPosition( newPosition: Vector2d ): void {
            this.position = newPosition;
            this.updateParentPosition();
        }

        public function getPosition(): Vector2d {
            return this.position;
        }

        public function addPosition( deltaPosition: Vector2d ): void {
            this.setPosition( this.getPosition().add( deltaPosition ) );
        }

        public function centreTo( object: SpaceObject ): void {
            this.setPosition( object.getPosition() );
        }

        private var connectedObject: SpaceObject = null;
        public function connectTo( object: SpaceObject ): void {
            this.connectedObject = object;
            this.parent.addEventListener( Event.ENTER_FRAME, this.connectionHandler );

            this.centreTo( connectedObject );
        }

        private function connectionHandler( e: Event ): void {
            if (!this.connectedObject) this.disconnect();

            this.centreTo( connectedObject );
        }

        public function disconnect(): void {
            this.connectedObject = null;
            this.parent.removeEventListener( Event.ENTER_FRAME, this.connectionHandler );
        }

        // scale part
        public function setScale( newScale: Number ): void {
            this.scale = Math.max(Math.min(newScale, maxScale), minScale);
            this.updateParentScale( this.scale );
        }

        public function getScale(): Number {
            return this.scale;
        }

        public function addScale( deltaScale: Number ): void {
            var newScale: Number = this.getScale() + deltaScale;

            this.setScale( newScale );
        }

        public function mulScale( scaleFactor: Number ): void {
            var newScale: Number = this.getScale() * scaleFactor;

            this.setScale( newScale );
        }

        public function zoomTo( object: SpaceObject ): void {
            var newScale: Number = object.scaleX; //TODO: use wigth and height of object with scale, not only scale

            this.setScale( newScale );
        }

	}
}
