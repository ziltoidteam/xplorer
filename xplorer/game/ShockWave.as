﻿package xplorer.game {
	import flash.display.MovieClip;
    import xplorer.system.Vector2d;
	import flash.utils.Timer;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
    import xplorer.game.objects.SpaceObject;
    import xplorer.game.requests.GameRequest;
    import xplorer.game.Lut;

	/**
	 * Object simulates shock wave, when some objects destructs or something.
	 * All objects interacting with shockwave got damage.
	 */
	public class ShockWave extends MovieClip{
		// Wave expands with constant velocity so not all objects interacting with it at the same time.
		var timers:Vector.<Timer> = new Vector.<Timer>;
		// Center of shock wave.
		var center: Vector2d;
		// Maximum of the damage in the center.
		var peakDamage: Number;
		// Effective damage radius.
		var expansionRadius: Number;
		// Wave's expancion velocity in distance per milisec.
		const expansionVelocity:Number = 0.7; //per millisec

		/**
		 * Initializes shock wave with given parameters.
		 * @param center Wave's center
		 * @param peakDamage Damage at the wave's center
		 * @param expansionRadius Effective wave's radius
		 */
		public function ShockWave(center: Vector2d, peakDamage: Number, expansionRadius: Number) {
			if(center == null || peakDamage<0)
				throw "ShockWave: incorrect parameters";

			this.addEventListener( Event.ADDED_TO_STAGE, onInitListener );
			this.addEventListener( Event.REMOVED_FROM_STAGE, onRemovedListener );

			this.center = center;
			this.peakDamage = peakDamage;
			this.expansionRadius = expansionRadius;
		}

		private function onInitListener(e: Event): void{
			// the only way to querry game about some objects around it is to give some space object.
			const virtualObject: SpaceObject = new SpaceObject(0, 0, this.center, 0, new Vector2d(0,0), 0, new Vector2d(0,0), 0, 0, 0, 0, 0, new Lut() );
			this.dispatchEvent(new GameRequest(GameRequest.searchNearObjects, virtualObject, setObjects));
		}

		private function onRemovedListener(e: Event): void{
			// empty
		}

		/**
		 * This function is called by the game to set all the dynamics of wave interaction.
		 * All objects in this list will possibly affected in damage. The closer object to the center,
		 * the much more and sooner it will be affected.
		 * @param objects
		 */
		private function setObjects(objects: Vector.<SpaceObject>): void{
			/* We filter all the objects to whom wave will be propogated more then in 1 sec.
			 * The damage get lower as a quadratic law.
			 * For singularity in the center we cut damage to maximum value.
			 * Minimum allowed damage is 1.
			 */
			for each(var object in objects){
				const distance = object.getPosition().getDistanceTo(this.center);
				const timeToReach: Number = (distance)/expansionVelocity;
				const damage = Math.min(this.peakDamage, this.peakDamage / Math.pow(distance/expansionRadius,2))

				if( timeToReach < 1000 && damage >= 1) {
					var pushTimer = new Timer(timeToReach);
					pushTimer.addEventListener(TimerEvent.TIMER, shockReachListener(object, damage));
					pushTimer.start();

					timers.push(pushTimer);
				}
			}
			// If no objects affected we need to call for self destruction procedure for the wave object.
			selfDestructionIfFinish();
		}

		/**
		 * Remove given timer from timers list.
		 * @param timer Timer to remove.
		 */
		private function removeTimer(timer: Timer): void{
			timer.stop();
			var index = timers.indexOf(timer);
			if(index != -1)
				timers.splice(index,1);
		}

		/**
		 * Check if all timers are done and we can safelly remove the object.
		 */
		private function selfDestructionIfFinish(): void{
			if(timers.length <= 0 && this.parent){
					this.parent.removeChild(this);
				}
		}

		/**
		 * This method return function object have to be run when timer triggered and wave reached some object.
		 * @param object Space object to be affected by the wave
		 * @param damage The maximum damage wave can do.
		 */
		private function shockReachListener(object: SpaceObject, damage: Number): Function {
			// Event function object
			var result: Function = function(e: TimerEvent) {
				/* Make shock wave effect */
				//object.playBoomAnimation(10,0.1); // uncomment if you want boom animation when shock wave reach the object
				object.applyDamage(damage);

				const shockFactor: Number = 10 * damage;
				const shockVel: Vector2d = (object.getPosition().sub(center)).normalize().mul(shockFactor);
				object.setVelocity(object.getVelocity().add(shockVel));

				/* Remove timer from timer list check if this timer
				 * is the last one, so disconnect shock wave from the scene */
				const timer: Timer = (e.target as Timer);
				removeTimer(timer);
				selfDestructionIfFinish();
			}

			return result;
		}


	}
}
