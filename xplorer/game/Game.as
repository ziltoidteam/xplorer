﻿package xplorer.game {
    import flash.display.MovieClip;
    import xplorer.ui.wheels.*;

    import flash.events.Event;
	import flash.events.TimerEvent;
    import flash.events.KeyboardEvent;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;

    import xplorer.ui.indicators.RangeIndicator;
    import xplorer.ui.indicators.TextIndicator;

    import xplorer.ui.Tip;

    import xplorer.system.Angle;
    import xplorer.traces.*;
    import xplorer.parts.weapons.*;

	import flash.utils.Timer;
    import xplorer.system.Vector2d;
    import xplorer.game.camera.LayeredCamera;
    import xplorer.game.objects.SpaceObject;
    import xplorer.game.objects.MaintenanceQueue;
    import xplorer.game.requests.GameRequest;
    import xplorer.game.Lut;
    import xplorer.game.objects.Ship;

    import xplorer.system.FrameRateCounter;

        /**
         * Class to handle the world mechanics with all objects and their interactions.
         * It also handles in game interface and controlling the player during it's game.
         */
	public class Game extends MovieClip {
		// Interface elements
        public var playerShip: Ship;
        private var playerCamera: LayeredCamera;
        private var layers: Object = {"bg": new MovieClip(), "game": new MovieClip(), "ui": new MovieClip()};
        private var layersMoveFactor: Object = {"bg": 0.99, "game": 1, "ui": 0};
        private var layers_order: Vector.<String> = new <String>["bg", "game", "ui"];

		// World constants
		static public const maxSightRadius: Number = 800;
		static public const garbageCollectorRadius: Number = 1600;

		// Game elements
		var objects:MaintenanceQueue = new MaintenanceQueue();
		var director:Director = null;
		var garbageCollectorTimer: Timer = new Timer(1000);

		// Control elements
		var moveWheel: TouchMoveWheel = null;
		var weaponWheel: TouchWeaponWheel = null;

        // Indicators
        var hpIndicator: RangeIndicator = null;
        var fuelIndicator: RangeIndicator = null;
        var ammoIndicator: RangeIndicator = null;
        var fpsIndicator: TextIndicator = null;
        var scoreIndicator: TextIndicator = null;

		/**
		 * Connects object to game.
		 * From this moment it will be part of game's physics and interraction model.
                 * This function designed such way, objects never actually removed or added during it's work.
                 * Look MaintenanceQueue class code for more info.
		 * @param object The object to add.
		 */
		public function connect(object: SpaceObject): void {
			objects.add(object);
			this.layers["game"].addChild(object);
		}

		/**
		 * Removes object from game.
		 * From this moment it will be removed from game's physics and interraction model.
                 * This function designed such way, objects never actually removed or added during it's work.
                 * Look MaintenanceQueue class code for more info.
		 * @param object The object to remove
		 */
		public function disconnect(object: SpaceObject): void {
			if(object.parent) // TODO: wrong place
				object.parent.removeChild(object);
			objects.del(object);

			object = null;
		}

		/**
		 * Changes the velocities of two objects to define it's after-collision state.
		 * The main idea, beside the cute graphical effect, is to preven it's repeated collision
		 * in the next frame. It will cause the bug, when each collision will result in object's death.
		 * So, we change their velocities to opposite directions to make sure it will never happen.
		 */
		private function changeVelocities(first: SpaceObject, second: SpaceObject): void{
			const rVec: Vector2d = (second.getPosition().sub(first.getPosition())).normalize();

			// Paralel proection
			const firstPar: Number = first.getVelocity().scalarMul(rVec);
			const secondPar: Number = second.getVelocity().scalarMul(rVec);

			// Perpendicular
			const firstPer: Vector2d = first.getVelocity().sub(rVec.mul(firstPar));
			const secondPer: Vector2d = second.getVelocity().sub(rVec.mul(secondPar));

			// Interaction model. We use the simpliest one: Equal velocities in opposite directions.
			const V1: Number = Math.abs(firstPar);
			const V2: Number = Math.abs(secondPar);

			const Vafter: Number = (V1 + V2) / 2;

			// Set new velocities
			first.setVelocity(firstPer.add(rVec.mul(-Vafter)));
			second.setVelocity(secondPer.add(rVec.mul(Vafter)));
		}

		/**
		 * Checking interractions between all objects.
		 * This function designed such way, objects never actually removed or added during it's work.
		 * Otherwise we will have troubles writing its in this simple manner with two for cycles.
		 * Look MaintenanceQueue class code for more info.
		 */
		private function applyInteractions(): void {
			for(var i = 0; i < objects.getObjects().length; ++i)
				for(var j = i+1; j < objects.getObjects().length; ++j)
			{
				const a: SpaceObject = objects.getObjects()[i];
				const b: SpaceObject = objects.getObjects()[j];

				if(a.isInteractedWith(b)) {
				   /* Objects can interact with each other only if they have
					* different, non null owners. Read about getOwner() method in SpaceObject.
					*/
					if( a.getOwner() != null && a.getOwner() == b.getOwner()) continue;

					changeVelocities(a,b);

					a.applyCollision(b);
					b.applyCollision(a);
				}
			}
		}

        private function createUI(): void{
            createControlWidgets();
            createHPIndicator();
            createFuelIndicator();
            createAmmoIndicator();
            createFPSIndicator();
            createScoreIndicator();
        }

        private function removeUI(): void{
            removeControlWidgets();
            removeHPIndicator();
            removeFuelIndicator();
            removeAmmoIndicator();
            removeFPSIndicator();
            removeScoreIndicator();
        }

        private function createHPIndicator(): void{
            var hpIndicatorLength = 120;
            hpIndicator = new RangeIndicator("HP: ", hpIndicatorLength);
            this.layers["ui"].addChild(hpIndicator);

            const right: Number = stage.stageWidth;
            const top: Number = 0 + 30;
            hpIndicator.x = right - 150;
            hpIndicator.y = top;
        }

        private function createAmmoIndicator(): void{
            var ammoIndicatorLength = 120;
            ammoIndicator = new RangeIndicator("Ammo: ", ammoIndicatorLength);
            this.layers["ui"].addChild(ammoIndicator);

            const right: Number = stage.stageWidth;
            const top: Number = 0 + 90;
            ammoIndicator.x = right - 150;
            ammoIndicator.y = top;
        }

        private function createFPSIndicator(): void{
            fpsIndicator = new TextIndicator("FPS: ");
            this.layers["ui"].addChild(fpsIndicator);

            const right: Number = stage.stageWidth;
            const top: Number = 0 + 120;
            fpsIndicator.x = right - 150;
            fpsIndicator.y = top;
        }

        private function createScoreIndicator(): void{
            scoreIndicator = new TextIndicator("Score: ");
            this.layers["ui"].addChild(scoreIndicator);

            const right: Number = stage.stageWidth;
            const top: Number = 0 + 150;
            scoreIndicator.x = right - 150;
            scoreIndicator.y = top;
        }

        private function createFuelIndicator(): void{
            var fuelIndicatorLength = 120;
            fuelIndicator = new RangeIndicator("Fuel: ", fuelIndicatorLength);
            this.layers["ui"].addChild(fuelIndicator);

            const right: Number = stage.stageWidth;
            const top: Number = 0 + 60;
            fuelIndicator.x = right - 150;
            fuelIndicator.y = top;
        }

		private function createControlWidgets(): void{
			var radius: Number = stage.stageHeight / 4;
			var center: Vector2d;

			center = new Vector2d(radius, stage.stageHeight - radius);
			moveWheel = new TouchMoveWheel(stage, playerShip, center, radius);
			this.layers["ui"].addChild(moveWheel);
			stage.addEventListener(MoveWheelEvent.changing, onMoveWheelChanging);

			center = new Vector2d(stage.stageWidth - radius, stage.stageHeight - radius);
			weaponWheel = new TouchWeaponWheel(stage, playerShip, center, radius);
			this.layers["ui"].addChild(weaponWheel);
			stage.addEventListener(WeaponWheelEvent.fire, onWeaponWheelFire);
		}

        private function removeHPIndicator(): void{
            this.layers["ui"].removeChild(hpIndicator);
        }

        private function removeFuelIndicator(): void{
            this.layers["ui"].removeChild(fuelIndicator);
        }

        private function removeAmmoIndicator(): void{
            this.layers["ui"].removeChild(ammoIndicator);
        }

        private function removeFPSIndicator(): void{
            this.layers["ui"].removeChild(fpsIndicator);
        }

        private function removeScoreIndicator(): void{
            this.layers["ui"].removeChild(scoreIndicator);
        }

        private function removeControlWidgets(): void{
            this.layers["ui"].removeChild(moveWheel);
            stage.removeEventListener(MoveWheelEvent.changing, onMoveWheelChanging);
            this.layers["ui"].removeChild(weaponWheel);
            stage.removeEventListener(WeaponWheelEvent.fire, onWeaponWheelFire);
        }

		private function onMoveWheelChanging(e: MoveWheelEvent)
		{
			playerShip.getEngine().enableTorque(e.torque);
			playerShip.getEngine().enableForce(e.force);
		}

		private function onWeaponWheelFire(e: WeaponWheelEvent)
		{
			playerShip.shootWeapon(0);
		}

        private function create_layers() {
            for each( var layer_index: String in layers_order ) {
                this.addChild( this.layers[layer_index] );
            }
        }

    	public function Game(playerShip: Ship, director: Director) {
            create_layers();

			// Now director can use standart interface to communicate with game with events
            this.director = director;

    	    // Player Ship
            this.playerShip = playerShip;

    		this.addEventListener( Event.ADDED_TO_STAGE, onInitListener );
    		this.addEventListener( Event.REMOVED_FROM_STAGE, onRemovedListener );
    	}

        public function restart(): void {
            this.connect(this.playerShip);

            for each (var object: SpaceObject in this.objects.getObjects()) {
                if (object != this.playerShip)
                    this.disconnect(object);
            }
        }

		private function onInitListener( e: Event ) {
            this.addChild(this.director);
            this.connect(this.playerShip);

			this.addEventListener( Event.ENTER_FRAME, onEnterFrameListener );

			this.addEventListener( GameRequest.createObject, createObjectListener );
			this.addEventListener( GameRequest.removeObject, removeObjectListener );
			this.addEventListener( GameRequest.searchNearObjects, searchNearObjectsListener );
			this.addEventListener( GameRequest.checkIfDead, checkIfDeadListener );
			this.addEventListener( GameRequest.getPlayerShip, getPlayerShipListener );

			// Enable multitouch handling by flash
			Multitouch.inputMode=MultitouchInputMode.TOUCH_POINT;
			this.createUI();

			stage.addEventListener(KeyboardEvent.KEY_DOWN, onkDOWN);
			stage.addEventListener(KeyboardEvent.KEY_UP, onkUP);

			garbageCollectorTimer.addEventListener(TimerEvent.TIMER, garbageCollectorListener);
			garbageCollectorTimer.start();

			playerCamera = new LayeredCamera(this.layers, this.layersMoveFactor);
			playerCamera.connectTo(playerShip);

            var text: String = "Hello, player!\n";
            text += "Use WASD keys to move your ship.\n";
            text += "Use R key to shoot.\n";
            text += "Your target - survive, collect coins and kill enemies.\n";
            text += "Good luck!\n";
            var initialTip: Tip = new Tip("A test Tip", text, 13, 100, true);
            this.layers["ui"].addChild(initialTip);  //TODO: Temporary disable for testing
        }

        /**
         * For debuging purposes, we add alternative to multitouch keyboard handlers.
         * Handler for Keyboard pressing button event.
         */
        public function onkDOWN( e: KeyboardEvent ) {
            if ( e.charCode == 'w'.charCodeAt(0) ) {
                playerShip.getEngine().enableForce(1);
            } else if ( e.charCode == '['.charCodeAt(0) ) {
                playerCamera.addScale(-10);
            } else if ( e.charCode == ']'.charCodeAt(0) ) {
                playerCamera.addScale(+10);
            } else if ( e.charCode == 'd'.charCodeAt(0) ) {
                playerShip.getEngine().enableTorque(+1);
            } else if ( e.charCode == 'a'.charCodeAt(0) ) {
                playerShip.getEngine().enableTorque(-1);
            }else if ( e.charCode == 'r'.charCodeAt(0) ) {
				playerShip.shootWeapon(0);
            }else if ( e.charCode == '1'.charCodeAt(0) ) {	// Those 1,2,3 options added for debug only and will be removed in release.
				playerShip.setWeapon(Director.getRocketLauncher(100));
            }
			else if ( e.charCode == '2'.charCodeAt(0) ) {
				playerShip.setWeapon(Director.getMachineGun(100));
            }
			else if ( e.charCode == '3'.charCodeAt(0) ) {
				playerShip.setWeapon(Director.getMineShooter(100));
            }
        }

        /**
         * For debuging purposes, we add alternative to multitouch keyboard handlers.
         * Handler for Keyboard releasing button event.
         */
        public function onkUP( e: KeyboardEvent ) {
            if ( e.charCode == 'w'.charCodeAt(0) ) {
                playerShip.getEngine().disableForce();
            } else if ( e.charCode == 'd'.charCodeAt(0) ) {
                playerShip.getEngine().disableTorque();
            } else if ( e.charCode == 'a'.charCodeAt(0) ) {
                playerShip.getEngine().disableTorque();
            }
        }

        public static function create_bg(w: Number, h: Number): MovieClip {
            var prototype = new BackgroundPart();
            var width = prototype.width - 3;
            var height = prototype.height - 3;
            var result = new MovieClip();
            for (var x: Number = 0; x < Math.round(w / width); ++x) {
                for (var y: Number = 0; y < Math.round(h / height); ++y) {
                    var part = new BackgroundPart();
                    result.addChild(part);
                    part.x = x*width - w / 2;
                    part.y = y*height - h / 2;
                }
            }

            return result;
        }

        var tile_w: Number = 400;
        var tile_h: Number = 300;
        private function paste_new_bg(x_tile: Number, y_tile: Number) {
            var new_bg: MovieClip = Game.create_bg(tile_w,tile_h);
            new_bg.x = x_tile * tile_w;
            new_bg.y = y_tile * tile_h;

            this.layers["bg"].addChild(new_bg);
        }

        var old_x_tile = null;
        var old_y_tile = null;
        private function move_bg() {

            if (playerShip == null)
                return;

            var x_tile: Number = Math.round(playerShip.getPosition().x / tile_w);
            var y_tile: Number = Math.round(playerShip.getPosition().y / tile_h);

            if (old_x_tile == x_tile && old_y_tile == y_tile)
                return;

            var bg: MovieClip = this.layers["bg"];
            while (bg.numChildren > 0) bg.removeChildAt(0)

            paste_new_bg(x_tile - 1, y_tile - 1);
            paste_new_bg(x_tile - 1, y_tile);
            paste_new_bg(x_tile - 1, y_tile + 1);
            paste_new_bg(x_tile, y_tile - 1);
            paste_new_bg(x_tile, y_tile);
            paste_new_bg(x_tile, y_tile + 1);
            paste_new_bg(x_tile + 1, y_tile - 1);
            paste_new_bg(x_tile + 1, y_tile);
            paste_new_bg(x_tile + 1, y_tile + 1);

            old_x_tile = x_tile;
            old_y_tile = y_tile;
        }

        /**
         * Make all game per frame routines.
         */
    	private function onEnterFrameListener(event: Event) {
		// Checking interactions between objects.
    		this.applyInteractions();
    		// Make syncronization of objects with all newly added and removed objects.
    		this.objects.sync();

            this.move_bg();

            if (playerShip) {
                var playerHp: Number = playerShip.getHealthPoints() * 100 /
                    playerShip.getMaxHealthPoints();
                hpIndicator.setScroll(playerHp);
                hpIndicator.setName("HP " + int(playerShip.getHealthPoints()) + "/" + int(playerShip.getMaxHealthPoints()) + ": ");

                var playerFuel: Number = playerShip.getEngine().getFuel() * 100 /
                    playerShip.getEngine().getMaxFuel();
                fuelIndicator.setScroll(playerFuel);
                fuelIndicator.setName("Fuel " + int(playerShip.getEngine().getFuel()) + "/" + int(playerShip.getEngine().getMaxFuel()) + ": ");

                var playerAmmo: Number = playerShip.getActiveWeapon().getQuantity() * 100 /
                    playerShip.getActiveWeapon().getMaxQuantity();
                ammoIndicator.setScroll(playerAmmo);
                ammoIndicator.setName("Ammo " + playerShip.getActiveWeapon().getQuantity() + "/" + playerShip.getActiveWeapon().getMaxQuantity() + ": ");

                var playerScore: int = playerShip.getLut().getScore();
                scoreIndicator.setValue(String(playerScore));

                var fps: int = FrameRateCounter.getFrameRate();
                var fpsMax: int = stage.frameRate;
                fpsIndicator.setValue(fps + "/" + fpsMax);
            }
    	}

    	public function onRemovedListener(e: Event): void {
    	    this.removeEventListener( Event.ENTER_FRAME, onEnterFrameListener );

			stage.removeEventListener(KeyboardEvent.KEY_DOWN, onkDOWN);
    	    stage.removeEventListener(KeyboardEvent.KEY_UP, onkUP);

			garbageCollectorTimer.removeEventListener(TimerEvent.TIMER, garbageCollectorListener);
			garbageCollectorTimer.stop();

            this.removeUI();
    	}

		/**
		 * This method called by timer to remove objects located to far from player ship.
		 */
		private function garbageCollectorListener(e: Event): void {
			if(this.playerShip == null)
				return;

			for each(var object in objects.getObjects()){
				if(this.playerShip.getPosition().getManhattenDistanceTo(object.getPosition()) > garbageCollectorRadius) {
					this.disconnect(object);
                }
			}

		}

    	/*
    	 * Those callback methods handle the object's requests from the game and parses them.
    	 */

    	/**
    	 * Request for adding object to the game.
    	 * r.getCaller() - object who need to be added.
    	 * r.getBackMethod(Vector.<SpaceObjects>) - not used, probably null.
    	 */
    	private function createObjectListener( r: GameRequest ): void{
    		if(r.getCaller() == null)
    			return;
    		this.connect(r.getCaller());
    	}

    	/**
    	 * Request for remove an object from the game.
    	 * r.getCaller() - object who need to be removed.
    	 * r.getBackMethod(Vector.<SpaceObjects>) - not used, probably null.
    	 */
        private function removeObjectListener( r: GameRequest ): void{
            if(r.getCaller() == null)
                return;
            this.disconnect(r.getCaller());
        }

		/**
		 * Request to return to caller other all objects in his field of view.
		 * The caller object itself included as well to this list.
		 * r.getCaller() - object who want to check for it's neighbors.
		 * r.getBackMethod(Vector.<SpaceObjects>) - function should be called to return result to the caller.
		 */
    	private function searchNearObjectsListener( r: GameRequest ): void {
    		if(r.getCaller() == null || r.getBackMethod() == null)
    			return;

    		  /*
    		  * Get all objects in given radius to caller.
    		  * We add only those objects caller can interract with, not the decorative ones.
    		  */
    		var validObjects: Vector.<SpaceObject> = new Vector.<SpaceObject>();
    			for each(var object in objects.getObjects()){
    			var distanceToCaller: Number = r.getCaller().getPosition().getDistanceTo(object.getPosition());
    				if( object.getInteractionRadius() != 0 && distanceToCaller <= maxSightRadius )
    					validObjects.push(object);
    			}

    			// Return list to the caller.
    		r.getBackMethod()(validObjects);
    	}

    	/**
    	 * Request to return if object is valid object on the scene. If not, call a givent method
    	 * r.getCaller() - object who need to be checked for validity.
    	 * r.getBackMethod() - function should be called to tell caller this object not valid.
    	 */
    	private function checkIfDeadListener( r: GameRequest ): void {
    		if(r.getCaller() == null || r.getBackMethod() == null)
    			return;

    		if(objects.getObjects().indexOf(r.getCaller()) == -1)
    		{
    			r.getBackMethod()();
    		}
    	}

		private function getPlayerShipListener( r: GameRequest ): void {
			if( r.getBackMethod() == null)
				return;

			r.getBackMethod()(this.playerShip);
		}

    }
}
