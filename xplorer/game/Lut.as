﻿package xplorer.game {
	/**
	 * Class to hold lut object holds in.
	 * For example, in the case of some ship destruction, it's lut can be moved to some other
	 * object, for example Score, flying in the space and waiting any other ship collects it. 
	 * REMARK: by now, the only lut is ship's score.
	 * In the future, vector of ShipParts will be added.
	 */
	public class Lut {
		var score: Number = 0;
		/* Fuel, cards, special bonuses;
		 */

		public function Lut() {
			// empty
		}

		/**
		 * Get score ship have.
		 */
		public function getScore(): Number{
			return this.score;
		}

		/**
		 * Set ship's score.
		 * @param score New score.
		 */
		public function setScore(score: Number): void{
			if( score < 0)
				throw "Lut: can't set negative score";

			// Score can't be float number.
			this.score = (int)(score);
		}

		/**
		 * Add score to current score.
		 * If result of this operation below zero, it is set to 0.
		 * @param delta Ammount of score to add. It can be negative.
		 */
		public function addScore(delta: Number): void{
			const newScore = Math.max(0, getScore() + delta);
			setScore(newScore);
		}

		/**
		 * Add content of other Lut object to this one.
		 */
		public function add( lut: Lut ): void{
			if( lut == null )
				return;

			this.addScore( lut.getScore() );
		}

		/**
		 * Clone lut object.
		 */
		public function clone(): Lut{
			var lut: Lut = new Lut();
			lut.setScore( this.getScore() );

			return lut;
		}

	}

}
