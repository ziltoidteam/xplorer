﻿package  {
    import flash.display.MovieClip;
    import xplorer.ui.windows.*;
    import xplorer.system.Debug;
    import xplorer.system.FrameRateCounter;

    public class Main extends MovieClip {
        public function Main() {
            Debug.set_debug_mode("on");
            WindowSystem.start(stage);
            FrameRateCounter.init(stage);

            var main: Window = new MainWindow();
            main.activate();
        }
    }
}
